clearvars;

MIN_X = -400.0;
MIN_Y = -400.0;
MAX_X = 400.0;
MAX_Y = 400.0;

DIVISIONS = 30.0;

STEP_X = 800.0 / DIVISIONS;
STEP_Y = 800.0 / DIVISIONS;

OBSTACLES = [150.0 150.0 150.0 90.0 90.0 90.0 90.0 150.0;
150.0 210.0 150.0 150.0 90.0 150.0 90.0 210.0;
210.0 150.0 210.0 90.0 150.0 90.0 150.0 150.0;
150.0 -90.0 150.0 -150.0 90.0 -150.0 90.0 -90.0;
210.0 -90.0 210.0 -150.0 150.0 -150.0 150.0 -90.0;
150.0 -150.0 150.0 -210.0 90.0 -210.0 90.0 -150.0;
-90.0 -90.0 -90.0 -150.0 -150.0 -150.0 -150.0 -90.0;
-90.0 -150.0 -90.0 -210.0 -150.0 -210.0 -150.0 -150.0;
-150.0 -90.0 -150.0 -150.0 -210.0 -150.0 -210.0 -90.0;
-90.0 150.0 -90.0 90.0 -150.0 90.0 -150.0 150.0;
-90.0 210.0 -90.0 150.0 -150.0 150.0 -150.0 210.0;
-150.0 150.0 -150.0 90.0 -210.0 90.0 -210.0 150.0;
10.0 60.0 10.0 -60.0 -10.0 -60.0 -10.0 60.0;];
         
% OBSTACLES = [100.0 42.5 142.5 0.0 100.0 -42.5 57.6 5.2;
%              -100.0 42.5 -57.6 0.0 -100.0 -42.5 -142.5 5.2;
%              2.6 142.4 42.5 100.0 2.6 57.6 -42.5 100.0;
%              2.6 -57.6 42.5 -100.0 2.6 -142.5 -42.5 -100.0;];

         
REPULSE_S = 20.0;
TANGENT_S = REPULSE_S + 20.0;
         
GOAL = [-300.0 -10.0 20.0 20.0];

ALPHA = 0.01; % Scalar for Goal Attractive Field
BETA = 0.06; % Scalar for repulsive fields
GAMMA = 0.03; % Scalar for tangentials

MIN_CLAMP = -1.0;
MAX_CLAMP = 1.0;

% ALPHA = 1.0; % Scalar for Goal Attractive Field
% BETA = 6.0; % Scalar for repulsive fields
% GAMMA = 3.0; % Scalar for tangentials
% 
% MIN_CLAMP = -800.0;
% MAX_CLAMP = 800.0;

[X,Y] = meshgrid(MIN_X:STEP_X:MAX_X,MIN_Y:STEP_Y:MAX_Y);
[U,V] = meshgrid(MIN_X:STEP_X:MAX_X,MIN_Y:STEP_Y:MAX_Y);

for xIdx=1:size(X)
    for yIdx=1:size(Y)
        tX = X(xIdx, yIdx);
        tY = Y(xIdx, yIdx);
        
        diffX = GOAL(1) - tX;
        diffY = GOAL(2) - tY;
        distance = sqrt(diffX^2 + diffY^2);
        theta = atan2(diffY, diffX);

        sX = clamp(ALPHA * distance * cos(theta), MIN_CLAMP, MAX_CLAMP);
        sY = clamp(ALPHA * distance * sin(theta), MIN_CLAMP, MAX_CLAMP);

        for oIdx=1:size(OBSTACLES)
            center = [mean(OBSTACLES([oIdx],1:2:end)), mean(OBSTACLES([oIdx],2:2:end))];
            RADIUS = sqrt((center(1) - OBSTACLES(oIdx,1))^2+((center(2) - OBSTACLES(oIdx,2))^2));
            diffX = center(1) - tX;
            diffY = center(2) - tY;
            dist = sqrt(diffX^2 + diffY^2);
            theta = atan2(diffY, diffX);
            if dist < (RADIUS + REPULSE_S)
%                 sX = sX - sign(diffX)*(abs(diffX) * BETA);
%                 sY = sY - sign(diffY)*(abs(diffY) * BETA);
                sX = sX - clamp(BETA * (REPULSE_S + RADIUS - dist)*cos(theta), MIN_CLAMP, MAX_CLAMP);
                sY = sY - clamp(BETA * (REPULSE_S + RADIUS - dist)*sin(theta), MIN_CLAMP, MAX_CLAMP);
            end
            
            if dist < (RADIUS + TANGENT_S)
                % Add the tangential component
                % Compute the vectors
                OV = [center(1) - GOAL(1), center(2) - GOAL(2)];
                TV = [tX - GOAL(1), tY - GOAL(2)];
                result = OV(1)*TV(2) - OV(2)*TV(1);

                sX = sX - clamp(GAMMA * (TANGENT_S + RADIUS - dist)*cos(theta + sign(result)*(pi/2)), MIN_CLAMP, MAX_CLAMP);
                sY = sY - clamp(GAMMA * (TANGENT_S + RADIUS - dist)*sin(theta + sign(result)*(pi/2)), MIN_CLAMP, MAX_CLAMP);
            end
        end
        
        U(xIdx, yIdx) = sX;
        V(xIdx, yIdx) = sY;
    end
end

% U = clamp((GOAL(1) - X) / SCALE, MIN_CLAMP, MAX_CLAMP);
% V = clamp((GOAL(2) - Y) / SCALE, MIN_CLAMP, MAX_CLAMP);

figure

hold on
quiver(X,Y,U,V)

for r = 1:size(OBSTACLES)
%    rectangle('Position', OBSTACLES([r],:),'FaceColor', 'y')
    fill(OBSTACLES([r],1:2:end), OBSTACLES([r],2:2:end), 'y');
%     vx = OBSTACLES(1:2:end);
%     vy = OBSTACLES(2:2:end);
end
rectangle('Position', GOAL, 'FaceColor', 'g', 'Curvature', [1,1])

axis([MIN_X MAX_X MIN_Y MAX_Y]);
axis square



