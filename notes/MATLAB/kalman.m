% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% Kalman Filtering MATLAB Tutorial   %%%
% %%% Michael A. Goodrich                %%%
% %%% 13 Oct 2004, CS 470                %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% GOAL                               %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % The goal of this tutorial is to help you understand how to use the Kalman filter
% % to estimate the state of a system.  Recall that the Kalman filter uses the
% % following equations.
% %
% %   PREDICTION/SYSTEM: P(x_{t+1}|(x_t) = N(Fx_t,\Sigma_x)(x_{t+1})
% %   OBSERVATION/SENSOR: P(z_t|x_t) = N(Hx_t,\Sigma_z)(z_t)
% %
% % where x=state, z=observation, p(x|z) is the posterior estimate of the state,
% % p(x) is the prior estimate of the state, and p(z|x) is the likelihood.  Our
% % goal is to estimate P(x_t|z_t) in some way using Bayes rule.  Recall that the
% % equation for Bayes rule is:
% %
% %                P(z_t|x_t) P(x_t)
% % P(x_t|z_t) = ---------------------
% %                       P(z_t)
% %
% % How do we connect the prediction and observation stuff to this equation for
% % Bayes rule?  We'll first give some background, then handle observations, and the
% % handle predictions.
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% C1: Estimating Parameters          %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %
% % In the BayesEstimator MATLAB tutorial and in the lab, we used the following
% % approach to implement an approximate Bayes rule.  It is only approximate because
% % we discretize a continuous state space --- the complete approach would require 
% % us to specify an entire probability density function across all possible continous
% % states.
% %
% % 1. Discretize the world, {x1,x2, ... , xn}.
% % 2. Assign a prior probability to each possible state {p(x1),p(x2), ... p(xn)}.  (Some of these may be zero.)
% % 3. Apply Bayes rule to each state, ignoring the normalizing factor
% %       for (i=1:n)
% %           p(xi|z) = p(z|xi) * p(xi)
% % 4. Normalize
% %       s=0;
% %       for (i=1:n)
% %           s=s+p(xi|z)
% %       for (i=1:n)
% %           p(xi|z) = p(xi|z)/s;
% % 
% % As you hopefully noticed in the previous section, the precision of the results
% % and the accuracy of the solution depends on how you discretized the world
% % and on how you chose your priors.  It would be good if we could make this a
% % bit more general.
% %
% % We choose to do this by using full pdfs over continuous states.  Clearly, we
% % can't create a table over such a space, but we can write a function that
% % represents the pdf over all the states.  
% %
% % The most well known function for doing this is the Gaussian, or Normal, 
% % function.  You've seen this before in the Bayesian tutorial and lab.
% %
% % Given that the world is continuous, we can apply Bayes rule by estimating 
% % the parameters of the Gaussian function as we accumulate observations.  
% % How? By applying Bayes rule and finding out how the parameters change
% % as a function of Bayes rule.
% %
% % I now want to show how estimating the parameters of a Gaussian function
% % can be done.  We begin in the same place as we did in the Bayes Tutorial.
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% C2: Corrupted readings of state    %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Consider a world with only one state, and suppose that this state is unknown.
% % We'll record the true state here.  This state is a point in a plane.
% s=[3;5];  % the point in a plane is represented by a column vector.
% 
% % Suppose that our goal is to estimate this true state because knowing the true
% % state will allow us to make some kind of decision.  Suppose further that we
% % have a sensor that can identify the true state, but that this sensor is very
% % noisy.
% 
% % To make this concrete, suppose that the true state of the world that we
% % are trying to estimate is the position of our flag in the capture the flag
% % world.  Suppose that we can identify our flag by, say, sonars, but that the
% % values returned by the sonars are noisy.
% 
% % Let's plot the location of the flag, and then show what noisy estimates
% % look like around the flag.
% figure(1);
% h=plot(s(1),s(2),'ro');  % Plot the flag as a red circle.
% set(h,'markersize',6,'linewidth',3); % make the circle big.
% axis([0,10,0,10]); % Set the scale of the plot so that we can see the origin.
% hold on;
% L = 100;
% n=2*randn(2,L); % Create a 100-sample noise sequence with a standard deviation of 2.
% x=zeros(2,L);
% for (i=1:L) 
%    x(:,i)=s+n(:,i);  % Add the noise to the true state to create 100 observations of the true state.
%    plot(x(1,i),x(2,i),'k.');    
% end;
% hold off;
% 
% % break;
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% C3: Estimating parameters          %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Probably the easiest way of estimating s is to take the average of the x's.
% % Let's see what this looks like.
% sest=mean(x')';  % The ' indicates a transpose.  Because the 'mean' function takes the
%                % average over the columns, I swap things around to get it to work.
% hold on;
% plot(sest(1),sest(2),'bs');  % Plot the average.
% hold off;
% % Run this several times to see how the estimate changes depending on which 
% % samples of noise occur.
% %
% % It turns out that the value of sest is the best estimate of the mean of a
% % Gaussian density that I can get.
% % 
% % We can also estimate the covariance matrix from our data.  
% K = cov(x');
% % 
% % Given this estimate of the mean and covariance, I have all the information
% % that I can possibly have about the parameters of the Gaussian function.  We
% % can plot them on the figure.
% %
% px = [-10:.1:10];
% py = [-10:.1:10];
% [X,Y]=meshgrid(px,py);
% f = zeros(length(px),length(py));
% for (i=1:length(px))
%    for (j=1:length(py))
%        v = [X(i,j);Y(i,j)];
%        f(i,j) = 1/sqrt((2*pi)^2*det(K)) * exp(-(v - sest)'*inv(K)*(v - sest)/2); 
%    end;
% end;
% hold on;
% contour(px,py,f);
% hold off;
% 
% % Run this a couple of times and watch how the contour lines of the Gaussian
% % density align with the scatterplot of the data.
% %
% % What would happen to the variance of the estimated Gaussian density function
% % if you added more noisy samples of s?  Try it and see.  What happened to the
% % mean?  What happens if you change the standard deviation of the noise?
% 
% % break;
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% C4: An iterative estimate          %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Sometimes, it is nice to compute an estimate as readings come in.
% % This way, you don't have to sit for a long time waiting for enough
% % observations to come in before you make a decision.
% %
% % To illustrate this, suppose that my goal is to reach the flag quickly
% % but that I only get an observation every tenth of a second.  This means
% % that it would take 10 seconds to get the estimate if I require 100 samples.
% % 
% % Is there a way to create an iterative estimate of the mean?  Yes.
% % Is there a way to create an interative estimate of the covariance?  Yes. 
% % 
% % The Kalman filter is an iterative estimate of the mean and covariance 
% % when the world acts in certain ways.  The goal of the next several steps
% % of this tutorial is to help you understand how the Kalman filter works.
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% C5: System Noise                   %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Recall that the equation for Bayes rule is:
% %
% %                P(z_t|x_t) P(x_t)
% % P(x_t|z_t) = ---------------------
% %                       P(z_t)
% %
% % For the most part, we can ignore P(z_t) --- we'll treat it like a 
% % simple normalizing function.  Thus, we will just need to pay attention
% % to the likelihood and priors.
% %
% % In the BayesEstimator tutorial, we took a simple approach to estimating P(x_t) ---
% % we just assumed that the prior estimate of state x at time t was given by the
% % posterior estimate of state x at time t-1 given the evidence to that point, 
% % P(x_{t_1}|z_{t-1}.
% % In other words, we assumed that the state didn't change between when we estimated
% % it at time t-1 and when we got a new estimate.  Thus,
% %
% % P(x_t) = P(x_{t-1}|z_{t-1}).
% %
% % When is this a good estimate?  When we have no reason to suppose that the state
% % changes.  In the example above, the true state did not change so we were right.
% % The Kalman filter generalizes this to a case when the state can change in a linear
% % fashion.
% %
% % Why is this important?  Because things in the real world change.  Consider the
% % following silly example.  Suppose that you are floating in an inner tube in the
% % middle of a big lake which is perfectly still.  Every few seconds, a fish swims 
% % from a random direction and crashes into your inner tube.  What will your position 
% % be after the fish crashes into you t times?
% %
% % We can model the suicidal fish problem using vector algebra.  Let $x_0$ be a vector
% % that represents your original position.  Thus, $x_0=[x_0;y_0]$.  Note that I have
% % overloaded the variable name $x$; it sometimes represent a vector and sometimes
% % represents the "x-position" in Euclidean space.  I apologize for this, but I want 
% % you to be able to think about x and y positions, and I also want to use the notation
% % in the book.  Hopefully, knowing about this overloading will allow you to handle it.
% %
% % Continuing with our model.  In the absence of fish, the vector algebra equation that
% % describes the location of the inner tube over time is:
% %
% % x_{t+1} = x_t.
% %
% % If we add in the perturbations from the suicidal fish then we get:
% %
% % x_{t+1} = x_t + nu_t.
% % 
% % We call nu_t the process, system, or plant noise.
% % 
% % Assuming Gaussian fish, we can say that nu_t is distributed with some mean, mu,
% % and some variance, sigma^2.  We write this as:
% %
% % nu_t ~ N(mu,sigma^2).
% %
% % We now come to the important question.  If we don't have any observations, what
% % happens to the quality of our estimate for state x as time goes by.  The answer
% % is that the quality of the estimate gets pretty bad pretty quickly.  The reason
% % is that without observations, all I can do is take my first guess and stick with that
% % even though I know the inner tube is moving --- I simply have no clue where the
% % suicidal fish are taking me.
% 
%     % figure(2); % Switch to a new figure window.
%     % 
%     % sest=s; % let the first estimate be the true state.  Draw it.
%     % plot(sest(1),sest(2),'ro');
%     % axis([-20,20,-20,20]);
%     % hold on;
%     % x=s;
%     % for (i=1:20)
%     %    x = x + 1.5*randn(2,1);  % nu ~ N(0,1.5^2).  The true state flops around.
%     %    plot(x(1),x(2),'ks');  % Plot the true state.
%     %    %pause(0.25);
%     %    drawnow;
%     % end;
%     % hold off;
% 
% % You should see a plot where the red circle indicates the estimate, and
% % the black squares blip onto the plot as fish bump into the inner tube.
% % Run this a couple of times, and note how the estimated value stays the same
% % even though the true value is flopping all around.
% 
% % break;
% 
% % One lesson that you should take from this is that if we are going to predict
% % the value of the state, this prediction is going to be noisy which means
% % that it is going to have a distribution.  This probability is denoted by
% % P(x_{t+1}|x_t).  What does it look like?  Well, if nu has zero mean and
% % if nu has a variance of, say sigma_x, then our prediction density function
% % follows the following:
% %
% % P(x_{t+1}|x_t) ~ N(x_t,sigma_x).
% %
% % Why is the mean x_t?  Because the noise has zero mean, which means that the
% % one step ahead prediction must have a mean of the previous state.  The variance
% % is sigma_x because knowing the previousx state (the previous state is given ---
% % we know this because we wrote the equation as a conditional density) means that
% % all of the variance comes from the system noise.  Compare this equation to the
% % second equation on page 586.  Note that the above equation and the equation in 
% % the book say the same thing.
% %
% % How do we use this in Bayes rule?  We use it as the prior value.  In other words,
% % given Bayes rule,
% %
% %                P(z_t|x_t) P(x_t)
% % P(x_t|z_t) = ---------------------
% %                       P(z_t)
% %
% % We set the prior at time t, P(x_t), equal to our predicted value of the state
% % given the previous state, P(x_t|x_{t-1}).  More on this later.
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% C6: Observation noise              %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % We've already dealt a great deal with observation noise.  Observation
% % noise is the noise that corrupts my readings.  If we let $z_t$ denote
% % the observation, then observation noise is given by the vector algebra
% % equation
% %
% % z_t = x_t + e_t,   e_t ~ N (mu, sigma_z^2)
% %
% % Suppose that mu=0.  What does the density of z_t look like given x_t?
% %
% % P(z_t|x_t) ~ N(x_t,sigma_z^2).
% %
% % Compare this to the third equation on page 586 of the book.  Note how 
% % they are the same.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% C7: System and Observation noise   %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Returning to Bayes rule,
% %
% %                P(z_t|x_t) P(x_t)
% % P(x_t|z_t) = ---------------------
% %                       P(z_t)
% %
% % what does P(x_t|z_t) look like?  Given that P(z_t|x_t) is Gaussian and
% % that P(x_t) is Gaussian, it just so happens that P(x_t|z_t) is also Gaussian.
% % Equation (15.22) of the book gives equations that show how we can take 
% % old estimates of the mean and variance parameters and produce new estimates.
% % Notice the presence of sigma_x and sigma_z.  Notice also the presence of
% % mu_t and sigma_t.
% %
% % What are mu_{t+1} and sigma_{t+1}?  They are the parameters of the posterior
% % density of x_{t+1} given z_{t+1},
% %
% % P(x_{t+1}|z_{t+1}) ~ N(mu_{t+1}, sigma_{t+1}).
% % 
% % We need an example to make this clear.  First, look at the first equation on
% % page 586.  This equation represents the density of our very first guess --- it
% % is our first prior.  Given our first state, let's select our first estimate from
% % this prior.
% %
% figure(3);
% x=s;
% plot(x(1),x(2),'ks');  % Black squares represent the true state.
% axis([-20,20,-20,20]);
% hold on;
% 
% %sig0 = 10.5;
% sig0 = 1.5
% sest = s + 10.5 * randn(2,1); 
% % sest = s + 1.5 * randn(2,1);  % Pick our first guess from our original density.
% plot(sest(1),sest(2),'ro');  % Red circles represent guesses.
% z=s; % This isn't really the zeroeth observation.  I'm including it to help with the legend.
% plot(z(1),z(2),'b.');  % Blue dots represent observed state
% xp=s; % This isn't really the zeroeth prediction.  I'm including it to help with the legend.
% plot(xp(1),xp(2),'m*');
% 
% 
% % We will cheat a bit here.  Because we know that the noises for the x and y axes
% % are uncorrelated, we can be sure that our covariance matrix is diagonal.
% S = [sig0^2,0;0,sig0^2];
% 
% 
% px = [-20:1:20];
% py = [-20:1:20];
% [X,Y]=meshgrid(px,py);
% f = zeros(length(px),length(py));
% for (i=1:length(px))
%    for (j=1:length(py))
%        v = [X(i,j);Y(i,j)];
%        f(i,j) = 1/sqrt((2*pi)^2*det(S)) * exp(-(v - sest)'*inv(S)*(v - sest)/2); 
%    end;
% end;
% hold on;
% 
% %%%%%%%%%%%%%%%%
% % ERROR NOTICE %
% %%%%%%%%%%%%%%%%
% 
% % If using MATLAB 7 or higher, replace the following line with
% %[c,h]=contour('v6',px,py,f);
% 
% [c,h]=contour(px,py,f);  % This shows a contour of our Gaussian so that you can
% % get a sense of how fat our estimate is.
% % for (i=1:length(h));
% %    set(h(i),'Visible','off');
% % end;
% % set(h(3),'visible','on');
% 
% % Run this a bunch, using the updates.
% sigx=1.9;sigz=1.1;
% mut=s; sigt=sig0;
% 
% legend('True States','Guessed State','Observed State','Predicted State');
% 
% for (it=1:20);
%   xp=mut;  % Predict next state.
%   x = x + sigx^2 * randn(2,1);  % Add system noise.
%   z = x + sigz^2 * randn(2,1);  % Add observation noise.
%   sigt=sqrt(S(1,1));  % Squared elements are variances, non-squared are standard deviations. The covariance matrix, S, has squared elements, but sigt is a standard deviation  
%   tx= ((sigt^2+sigx^2)*z(1) + sigz^2*mut(1))/ (sigt^2+sigx^2+sigz^2); % x-component of mu vector
%   sigt=sqrt(S(2,2));
%   ty= ((sigt^2+sigx^2)*z(2) + sigz^2*mut(2))/ (sigt^2+sigx^2+sigz^2); % y-component of mu vector
%   mut=[tx;ty];
%   sigt=sqrt(S(1,1));
%   tx=(sigt^2+sigx^2)*sigz^2 / (sigt^2+sigx^2+sigz^2); % x-component of sigt vector
%   sigt=sqrt(S(2,2));
%   ty=(sigt^2+sigx^2)*sigz^2 / (sigt^2+sigx^2+sigz^2); % y-component of sigt vector
%   S = [tx,0;0,ty];
%   plot(x(1),x(2),'ks');  % Black squares represent the true state.
%   plot(z(1),z(2),'b.');  % Blue dots represent observed state
%   plot(mut(1),mut(2),'ro');  % Red circles represent guesses --- use the mean as the gues.
%   plot(xp(1),xp(2),'m*');
% 
%    for (ci=1:length(px))
%            for (cj=1:length(py))
%            v = [X(ci,cj);Y(ci,cj)];
%            f(ci,cj) = 1/sqrt((2*pi)^2*det(S)) * exp(-(v - mut)'*inv(S)*(v - mut)/2); 
%        end;
%    end;
%    hold on;
%    [c,h]=contour(px,py,f);  % This shows a contour of our Gaussian so that you can
%    % get a sense of how fat our estimate is.
%    for (ci=2:length(h));
%        set(h(ci),'Visible','off');
%    end;
%    %set(h(1),'visible','on');
% 
%    drawnow
% end;
% hold off;
% 
% % Try some different values for sigx and sigz.  Most importantly, try setting
% % sigx high and sigz low, and then do the opposite.  What happened?
% 
%  break;
% 
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% C8: Matrix models                  %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %
% % Hopefully, you've played with different values for the covariances in the
% % previous example.  You should have noticed that if the system noise is high
% % then your estimate should be based almost exclusively on the observation.
% % By contrast, if your observation noise is high then your estimate should be 
% % based almost exclusively on the system model.  The Bayesian estimate is
% % based on finding a balance between the observation and the system models.
% %
% % The Kalman filter generalizes the previous problem by allowing more
% % sophisticated system models and observation dynamics.  The basic idea
% % is to say that 
% % z_t = Hx_t + e_t,   e_t ~ N (mu, sigma_z^2)
% %
% % As before, suppose that mu=0.  What does the density of z_t look like given x_t?
% %
% % P(z_t|x_t) ~ N(Hx_t,sigma_z^2).
% %
% % Now, consider a more general system model
% %
% % x_{t+1} = Fx_t + nu_t,   nu_t ~ N (mu, sigma_x^2)
% % 
% % As before, suppose that mu=0.  What does the density of x_{t+1} look like
% % given x_t?
% % 
% % P(x_{t+1}|x_t) ~ N(Fx_t,sigma_x^2).
% %
% % Our equations now have matrices multiplying x_t.  What does this mean?
% 
% % To help answer this question, let's return to our suicidal fish problem.  
% % It is helpful to dig into the physics of this game a little bit more.  What
% % is really happening in this problem is that the fish swim up to the inner tube
% % and push on it for a few moments before leaving.  The question is what does
% % this do to the position and velocity of the inner tube?  
% %
% % Thanks to Newton, we know that the position after T seconds of constant acceleration
% % is given by:
% %
% % x_{t+1} = x_t + T v_t + T^2/2 a_t.
% %
% % Interestingly, our position at time t+1 is a function not only of our previous
% % position and the force put on the tube by the fish, but also on our previous
% % velocity.  Since velocity will also change, we should include this as part 
% % of our state and try to figure out how the fish change velocity.  Newton tells
% % us that the velocity after T seconds of constant acceleration is give by:
% %
% % v_{t+1} = v_t + T a_t.
% %
% % We can write this as a matrix equation. Let x_t = [x_t;v_t] (a column vector).
% % Let F = [1 T; 0 1] and G = [T^2/2;T].  Then
% %
% % x_{t+1} = F x_t + G a_t.
% %
% % In words, x_t represents a state vector consisting of position and velocity.  
% % The state vector at time t+1 is equal to the old state vector from time t stretched
% % and rotated by matrix F, plus the effects of the input vector.  Multiply these
% % matrix equations out and see if you get the right equations for x_{t+1} and
% % v_{t+1}.
% %
% % We have generated this matrix equation just by paying attention to the x-direction. 
% % If we include the y-direction, it just makes our matrices bigger.  Let ax_t denote
% % the acceleration in the x direction at time t, and let ay_t denote the acceleration
% % in the y-direction at time t.  Let vx_t and vy_t similiarly denote velocities in the
% % x and y directions, respectively.  Then the matrix equations are changed as follows.
% %
% % F = [1 T 0 0; 0 1 0 0; 0 0 1 T; 0 0 0 1];
% % G = [T^2/2 0; T 0; 0 T^2/2; 0 T];
% % x_t = [x_t;vx_t; y_t; vy_t] 
% % a_t = [ax_t; ay_t];
% %
% % x_{t+1} = Fx_t + Ga_t.
% %
% % Again, I encourage you to multiply the matrix equations out and see if you get
% % Newton's equations for motion in the x and y directions.
% %
% % Let's do a demo.
% figure(4);
% p=[3;5];  % Starting position.
% T=0.1; % Sample time
% v=[0;0];
% a=[3;0];  % constant acceleration
% 
% F = [1 T 0 0; 0 1 0 0; 0 0 1 T; 0 0 0 1];
% G = [T^2/2 0; T 0; 0 T^2/2; 0 T];
% x = [p(1);v(1); p(2); v(2)];
% plot(x(1),x(3),'k.');  % Plot the x and y components of the state.
% axis([-20,20,-20,20]);
% hold on;
% for (i=1:30)
%    x = F*x + G*a;
%    plot(x(1),x(3),'k.');  % Plot the x and y components of the state.
%    drawnow;
%    pause(.2);
% end;
% hold off;
% 
% % Notice how the state begins at (3,5) and moves along the x axis with ever-
% % increasing speed.  What happens if you change a?
% 
% % break;
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% C8: Observation matrix             %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % The preceding discussion leaves two questions unaswered.  First, what do we do with G ---
% % we didn't see a G in the equations for p(x_{t+1}|x_t) and p(z_t|x_t).  Second, where
% % does the H come from?  We'll answer the second of these questions in this section.
% %
% % Recall that p(z_t|x_t) is called the likelihood function and it represents what we know
% % about how we get observations about the true state of the world.  For our suicidal fish
% % problem, our besieged tube rider has a cell phone and wants to call for help.  Since the 
% % lake is big, he wants to be able to give the search and rescue people enough information
% % that they can figure out where he is.  He also has a GPS system which tells him his x and
% % y position on the lake.  
% %
% % Although his GPS system is cool, it doesn't give him precise readings (play along with me here
% % --- clearly, the positions are sufficient to allow search and rescue to find him on the lake;
% % pretend that they aren't).  Additionally, the GPS system does not give his velocity.
% %
% % We can write this ability to see just positions and not velocities using a matrix equation
% % as follows:
% %
% % x_t = [x_t;vx_t;y_t;vy_t]
% % z_t = [x_t;y_t]
% % H = [1 0 0 0; 0 0 1 0]
% %
% % z_t = Hx_t.
% %
% % Again, I encourage you to multiple out the matrix equations and see if we get the right things
% % for z_t.
% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% C9: Modelling the world            %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % The preceding discussion leaves the following question unanswered.  What do we do with G ---
% % we didn't see a G in the equations for p(x_{t+1}|x_t) and p(z_t|x_t). The complete Kalman
% % filter uses this and you may want to use it in the lab (but you'll have to be creative
% % because I won't tell you precisely how).  Instead, we bluff our way around G.
% %
% % Suppose that we don't really know when the fish will stop pushing (fish are, after all,
% % not that predictable).  Our best guess is that they will keep pushing with the same force
% % that they did the last time interval unless something stops them.  Thus, we guess that
% % a_{t+1} = a_t.  (These are tough fish if they can keep pushing with a constant force
% % even though they are travelling at 15 m/s.)  We can now create a new state and a new
% % matrix equation that describes our set of suicidal, though tough, fish.
% %
% % x_t = [x_t;vx_t;ax_t;y_t;vy_t;ay_t]
% % F = [1 T T^2/2 0 0 0; 0 1 T 0 0 0; 0 0 1 0 0 0; 0 0 0 1 T T^2/2; 0 0 0 0 1 T; 0 0 0 0 0 1]
% %
% % x_{t+1} = F x_t + nu_t.
% %
% % Now, our nu_t not only represents the randomness in the world, it also represents
% % unmodelled dynamics of our system (such as the fact that fish aren't as tough as
% % we suppose).  
% %
% % Reid, our illustrious TA, suggested that it would be good to introduce some friction
% % into the world.  Ideally, we'd like to have some wind resistance or water resistance
% % for our inner tube, but this resistance is quadratic in the velocity.  This means that
% % we can't represent this using a linear matrix equation.  
% %
% % We fake this out by supposing that v is probably small (and let nu_t compensate for
% % this weak assumption).  We set a_{t+1} = -0.1 v_t + a_t which means that acceleration
% % decreases with velocity.
% % 
% % The complete equations of motion used in the lab are:
% %
% % x_t = [x_t;vx_t;ax_t;y_t;vy_t;ay_t]
% % F = [1 T T^2/2 0 0 0; 0 1 T 0 0 0; 0 -0.1 1 0 0 0; 0 0 0 1 T T^2/2; 0 0 0 0 1 T; 0 0 0 0 -0.1 1]
% %
% % x_{t+1} = F x_t + nu_t.
% %
% %
% % Let's do a demo.
% figure(5);
% p=[3;5];  % Starting position.
% T=0.1; % Sample time
% v=[0;0];
% a=[2;0];  % constant acceleration
% F = [1 T T^2/2 0 0 0; 0 1 T 0 0 0; 0 -0.1 1 0 0 0; 0 0 0 1 T T^2/2; 0 0 0 0 1 T; 0 0 0 0 -0.1 1]
% 
% x = [p(1); v(1); a(1); p(2); v(2); a(2)];
% plot(x(1),x(3),'k.');  % Plot the x and y components of the state.
% axis([-20,20,-20,20]);
% hold on;
% for (i=1:30)
%    x = F*x + randn(6,1);
%    plot(x(1),x(3),'k.');  % Plot the x and y components of the state.
%    drawnow;
%    pause(.2);
% end;
% hold off;
% 
% % break;
% 
% % Notice how the state begins at (3,5) and moves randomly along.  What happens if 
% % you change nu?  One way to do this is to create a weighting matrix.  I'm going to
% % do this by creating a diagonal matrix with different values.  I want the acceleration
% % to be much more random than it currently is, so
% 
% S=[0.1 0 0 0 0 0; 0 0.1 0 0 0 0; 0 0 4 0 0 0; 0 0 0 0.1 0 0; 0 0 0 0 0.1 0; 0 0 0 0 0 4];
% figure(6);
% p=[3;5];  % Starting position.
% T=0.1; % Sample time
% v=[0;0];
% a=[2;0];  % constant acceleration
% F = [1 T T^2/2 0 0 0; 0 1 T 0 0 0; 0 -0.1 1 0 0 0; 0 0 0 1 T T^2/2; 0 0 0 0 1 T; 0 0 0 0 -0.1 1];
% 
% x = [p(1); v(1); a(1); p(2); v(2); a(2)];
% plot(x(1),x(3),'k.');  % Plot the x and y components of the state.
% axis([-20,20,-20,20]);
% hold on;
% for (i=1:30)
%    x = F*x + sqrtm(S)*randn(6,1);
%    plot(x(1),x(3),'k.');  % Plot the x and y components of the state.
%    drawnow;
%    pause(.2);
% end;
% hold off;
% 
% % break;
% 
% % Run this a couple of times and see what happens.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% C10: Some linear algebra review    %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % Since we're doing linear algebra anyway, I think it would be good to review
% % some basics like eigenvalues and eigenvectors.  I haven't decided whether I'll
% % test on this, but I hope that your curiosity will motivate you to read this.
% %
% % When we multiply a vector by a matrix, we are rotating and scaling the vector.
% % In the previous parts of this tutorial, we drew the vectors as points in
% % a space, but we could just have easily drawn them as lines.
% figure(7);
% x=[3;5];
% plot([0,x(1)],[0,x(2)]);
% axis([-20,20,-20,20]);
% 
% % To illustrate the principles of matrix multiplication, we'll use 2-element
% % vectors since we can plot these easily.  Let's create a matrix.
% A=[-.1 -.21; -.21 -.4];
% hold on;
% xp=A*x;
% plot([0,xp(1)],[0,xp(2)],'r--');
% hold off;
% % Notice how the original solid blue line, which represents the original vector,
% % points to the upper right.  Also notice how the the dashed red line points
% % to the lower left.  What happened?  The matrix rotated the vector.
% % 
% % Also notice that the dashed red line is shorter than the solid blue line.  
% % What happened? The matrix scaled the vector.
% %
% % Matrices rotate and scale vectors.
% %
% % The eigenvalues of a matrix A determine whether the vector x grows or shrinks when
% % multiplied by A.
% eig(A)
% % Since the eigenvalues of this matrix are both less than one, the vector shrinks.
% %
% % Consider another matrix.
% A=[2 -5; -5 -10];
% x=[3;5];
% plot([0,x(1)],[0,x(2)]);
% axis([-20,20,-20,20]);
% hold on;
% xp=A*x;
% plot([0,xp(1)],[0,xp(2)],'r--');
% hold off;
% % Notice how the dashed red line rotates and grows.  Why? because the eigenvalues
% % of A are larger than one.
% eig(A)
% 
% % Why are eigenvalues important?  Because they allow us to conclude things about how
% % an arbitrary vector will be affected when multipled by a matrix.  To understand this,
% % we can return to the definition of an eigenvector and an eigenvalue.  Recall that
% % v is an eigenvector and e is an eigenvalue for the matrix A if:
% %
% % Av = ev.
% % 
% % In words, an eigenvector is a special vector that is just scaled (not rotated!) when
% % multipled by the matrix A.  (One caveat, it might be rotated 180 degrees if the sign
% % of the eigenvalue is negative.)
% %
% % Let's test this.
% [V,e]=eig(A); % v is a matrix whose columns are eigenvectors --- type 'help eig'.
% v1=V(:,1);
% v2=V(:,2);
% plot([0,v1(1)],[0,v1(2)]);
% v1p=A*v1;
% hold on;
% plot([0,v1p(1)],[0,v1p(2)],'r--');
% hold off;
% % Notice how the new vector is just a scaled version of the previous vector, though
% % it does point in the opposite direction.  Try the other eigenvector.
% plot([0,v2(1)],[0,v2(2)]);
% v2p=A*v2;
% hold on;
% plot([0,v2p(1)],[0,v2p(2)],'r--');
% hold off;
% % In this case, the vectors point in the same direction, but one vector is longer than
% % the other.
% 
% % Recall that any arbitrary vector can be written as the weighted sum of linearly independent
% % vectors that span the space.  For this case, this means that if the two eigenvectors are
% % linearly independent, then we can take an arbitrary vector x and write it as the weighted 
% % sum of v1 and v2.  Some straightforward linear algebra gives
% x=[3;5];
% w = inv(V) * x; % w is our weighting matrix, x=w(1) v1 + w(2) *v2.
% w(1)*v1 + w(2)*v2  % Test
% % Plot
% plot([0,x(1)],[0,x(2)]);
% hold on;
% plot([0,w(1)*v1(1)],[0,w(1)*v1(2)],'r--');
% plot([w(1)*v1(1),w(1)*v1(1)+w(2)*v2(1)],[w(1)*v1(2),w(1)*v1(2)+w(2)*v2(2)],'r--');
% hold off;
% %
% % Now we can see what happens when we multiply x by A
% %
% % Ax = A* (w(1) v1 + w(2) *v2)   --- because x can be written as a weighted sum
% %    = A*w(1) v1 + A*w(2) v2     --- because I can distribute A over the weighted sum
% %    = w(1) A v1 + w(2) A v2     --- because I can commute a scalar with a matrix
% %    = w(1) e1 v1 + w(2) e2 v2   --- because A v1=e1 v1 and A v2=e2 v2 by the definition of an eigenvector
% %    = w'(1) v1 + w'(2) v2       --- by creating a new weight vector w'(1) = w1 e1.
% %
% % Thus, Ax is simply a new weighted sum of the eigenvectors.  Moreover, if the eigenvalues
% % are bigger than 1, this new weighted sum is larger than the original so the
% % weighted vector will grow bigger.  The converse is true if the eigenvalues are
% % smaller than 1.
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%% C11: The Kalman filter            %%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % The equations on page 588 (15.22) tell how mu and S are updated when 
% %
% % P(x_{t+1}|x_t) ~ N(Fx_t,Sx)
% % P(z_t|x_t ) ~ N(Gx_t,S_z)
% %
% Let's try and apply these equations to our suicidal fish problem.  We'll use the following
% model for the system.
figure(8)

Sx=5*[0.1 0 0 0 0 0; 0 0.1 0 0 0 0; 0 0 4 0 0 0; 0 0 0 0.1 0 0; 0 0 0 0 0.1 0; 0 0 0 0 0 4];
%Sx=1*[0.1 0 0 0 0 0; 0 0.1 0 0 0 0; 0 0 4 0 0 0; 0 0 0 0.1 0 0; 0 0 0 0 0.1 0; 0 0 0 0 0 4];  
   % I included a multiplicative factor at the start of this so that you can easily scale system and observation noise.
T=0.1; % Sample time
F = [1 T T^2/2 0 0 0; 0 1 T 0 0 0; 0 -0.1 1 0 0 0; 0 0 0 1 T T^2/2; 0 0 0 0 1 T; 0 0 0 0 -0.1 1];

p=[0;0];  % Starting position.
v=[0;0];  % Starting velocity.
a=[2;0];  % Starting acceleration
% initial state
x = [p(1); v(1); a(1); p(2); v(2); a(2)];

% We'll suppose that only the x and y positions can be observed.
H = [1 0 0 0 0 0; 0 0 0 1 0 0 ];  % z=[x;y] = H*x.
Sz=1*[1 0 ; 0 1];
%Sz=5*[1 0 ; 0 1];
   % I included a multiplicative factor at the start of this so that you can easily scale system and observation noise.

plot(x(1),x(4),'ks');  % Black squares represent the true state.
hold on;
axis([-20,20,-20,20]);
z=[0;0];

St=ones(size(Sx));  % Initialize St.
mut = x;              % Initialize mut.
Id = [1 0 0 0 0 0; 0 1 0 0 0 0; 0 0 1 0 0 0; 0 0 0 1 0 0; 0 0 0 0 1 0; 0 0 0 0 0 1];

for (i=1:20)
   xp=F*x;
   x = F*x + sqrt(Sx)*randn(length(x),1);
   z = H*x + sqrt(Sz)*randn(length(z),1);
   plot(z(1),z(2),'b.');  % Blue dots represent observed state
   plot(xp(1),xp(4),'m*'); % Magenta stars represent the predicted state.

   % Step 1: update kalman Gain.
   Kt = (F*St*F' + Sx)*H'*inv(H*(F*St*F'+Sx)*H'+Sz);
   % Step 2: update St.
   St = (Id-Kt*H)*(F*St*F' + Sx);
   % Step 3: Update mut.
   mut = F*mut + Kt*(z - H*F*mut);

   plot(mut(1),mut(4),'ro');  % Red circles represent guesses --- use the mean as the guess.
   plot(x(1),x(4),'ks');  %Black squares represent the true state.
   legend('True States','Observed State','Predicted State','Kalman State');
   drawnow;
   pause(.2)
end;
hold off;

% Run this a bunch of times with different scaling factors on Sx and Sz.
% Notice how the Kalman estimate is usually closer to the true value than either the
% observation or the predicted values.