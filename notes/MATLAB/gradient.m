% Just plots a gradient

x=[-1:.1:1];
y=[-2:.1:2];

[X,Y]=meshgrid(x,y);

f = -1*(3*X - 4*Y + 3*X.^2.*Y.^4);

surf(x,y,f+60);

[px,py] = gradient(f,.4,.4);

hold on;
quiver(x,y,px,py,5);
hold off;

