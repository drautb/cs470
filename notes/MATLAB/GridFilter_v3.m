%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% GridFilter.m                %
% Michael A. Goodrich         %
% 12 September 2007           %
%                             %
% Updated 24 October 2007     %
%                             %
% Tutorial and example code   %
% for applying Bayes rule to  %
% robot mapping.              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


WorldDim = 100;
ObstLen  = 10;
RobotLen = ObstLen/4;
SensorLen = ObstLen;
NumObst  = 5;


%%%%%%%%%%%%%%%%%%%%%%%%
% Likelihoods          %
% Updated Oct 24, 2007 %
%%%%%%%%%%%%%%%%%%%%%%%%
%
% For this problem, there are two possible states for each cell: occupied and unoccupied.
% For each cell, there are three possible observations: hit, no-hit, and no observation.
% Since we won't update probabilities in the absence of an observation, we will only use
% the hit or miss observations.
%
% We can give names for each likelihood as follows:
% p(O=hit|S=occupied) = "TrueHit"
% p(O=hit|S=unoccupied) = "FalseAlarm"
% p(O=no-hit|S=occupied) = "MissedDetection" = 1-TrueHit
% p(O=no-hit|S=unoccupied) = "TrueMiss" = 1-FalseAlarm

% TrueHit = 0.98;  % The probability that an occupied cell is detected as a hit by the observer.
% FalseAlarm = 0.6; % The probability that an unoccupied cell is detected as a hit.
TrueHit = 1;
FalseAlarm = 0.0;

%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE PROBABILITY GRID %
%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Create a data structure that holds the probability that the point
% specified by the index contains an obstacle.  Initialize it to a
% probability of 0.25 per each obstacle.
p = .75*ones(WorldDim,WorldDim);
% Plot the probability that a grid location is occupied by an obstacle as a
% pseudo-intensity -- darker colors indicate that the grid location is more
% likely to be occupied.
GridHandle = zeros(WorldDim,WorldDim);
clf;
hold on;
for (i=1:WorldDim)
    for (j=1:WorldDim)
        GridHandle(i,j) = plot(i,j,'.');
        set(GridHandle(i,j),'color',[1-p(i,j),1-p(i,j),1-p(i,j)],'MarkerSize',5,'EraseMode','normal');
    end;
end;
hold off;
set(gca,'DataAspectRatio',[1,1,1]);
axis([-20,120,-20,120]);
title('The probability that a cell is occupied');
xlabel('x');ylabel('y');
drawnow;

%%%%%%%%%%%%%%%%%%%
% PLACE OBSTACLES %
%%%%%%%%%%%%%%%%%%%
%
% Randomly select the x and y positions of the obstacles in the world.  
% Obst(i,:) is a vector of the x and y positions of the left corner of
% the obstacle.
Obst = (WorldDim-ObstLen)*rand(NumObst,2);  

% Plot the obstacles, but put them in a different figure
figure(3);clf;
for (i=1:NumObst)
    xvertices = [Obst(i,1),Obst(i,1),Obst(i,1)+ObstLen,Obst(i,1)+ObstLen];
    yvertices = [Obst(i,2),Obst(i,2)+ObstLen,Obst(i,2)+ObstLen,Obst(i,2)];
    
    patch(xvertices,yvertices,'r');
end;
set(gca,'DataAspectRatio',[1,1,1]);
axis([1,100,1,100]);
title('Actual locations of obstacles');
xlabel('x');ylabel('y');drawnow;
figure(1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CREATE GROUND TRUTH OCCUPANCY GRID %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% This will help me make observations easier.
Occupied = zeros(WorldDim,WorldDim);
for (i=1:WorldDim)
    for (j=1:WorldDim)
        for (k=1:NumObst)
            if ((i>=Obst(k,1) & i<=Obst(k,1)+ObstLen) & (j>=Obst(k,2) & j<=Obst(k,2)+ObstLen))
                Occupied(i,j)=1;
                %hold on;plot(i,j,'cx');hold off;  % Uncomment to debug ground truth.
            end;
        end;
    end;
end;

%%%%%%%%%%%%%%%%%%
% CREATE A ROBOT %
%%%%%%%%%%%%%%%%%%
offset = WorldDim/2;
xvertices = [-SensorLen+offset,SensorLen+offset,SensorLen+offset,-SensorLen+offset];
yvertices = [-SensorLen+offset,-SensorLen+offset,SensorLen+offset,SensorLen+offset];
sensor = patch(xvertices,yvertices,[.9,.9,.9]);
xvertices = [-RobotLen+offset,RobotLen+offset,RobotLen+offset,-RobotLen+offset];
yvertices = [-RobotLen+offset,-RobotLen+offset,RobotLen+offset,RobotLen+offset];
robot = patch(xvertices,yvertices,'m');



%%%%%%%%%%%%%%%%%%%%%%
% PERFORM SIMULATION %
%%%%%%%%%%%%%%%%%%%%%%
%
% Initialize robot movement.
vel = 3; % Velocity in units per sample.
theta = pi/4; % Initial direction of the robot
dt = 0.1;  % How often the robot's position is updated.
for (SimLen=0:5000) % For a simulation length of 5000 samples
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % ROBOT MOVEMENT MODEL  %
    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Retrieve the coordinates that define the robot
    xvertices=get(robot,'XData');
    yvertices=get(robot,'YData');
    sensorxvertices = get(sensor,'XData');
    sensoryvertices = get(sensor,'YData');
    theta = theta + (2*rand(1,1)-1)*dt;  % Perturb the direction a bit.
    if ((min(xvertices + vel*cos(theta))>=1) & (max(xvertices + vel*cos(theta))<=WorldDim)) % if within world boundaries
        xvertices = xvertices + vel*cos(theta);
        sensorxvertices = sensorxvertices + vel*cos(theta);
    else theta=2*pi*rand(1,1);
    end;
    if ((min(yvertices + vel*sin(theta))>=1) & (max(yvertices + vel*sin(theta))<=WorldDim)) % if within world boundaries
        yvertices = yvertices + vel*sin(theta);
        sensoryvertices = sensoryvertices + vel*sin(theta);
    else theta=2*pi*rand(1,1);
    end;
    set(robot,'XData',xvertices,'YData',yvertices);
    set(sensor,'XData',sensorxvertices,'YData',sensoryvertices);
    drawnow;
        
    %%%%%%%%%%%%%%%%%%%%%
    % OBSERVATION MODEL %
    %%%%%%%%%%%%%%%%%%%%%
    %
    % Generate a series of readings for the robot based on the locations of
    % obstacles. The location of the center of the robot, and therefore the
    % center of the sensor, is given by RobotX and RobotY.
    RobotX = round((max(xvertices)+min(xvertices))/2);  
    RobotY = round((max(yvertices)+min(yvertices))/2);
    
    Observation = zeros(2*SensorLen,2*SensorLen); % A zero means nothing detected
    for (i=1:2*SensorLen)
        for (j=1:2*SensorLen)
            SensorX=i+RobotX-SensorLen;SensorY=j+RobotY-SensorLen;
            %SensorX and SensorY contain the true world coordinates of the
            %sensor location.
            % Observation contains windowed observation, not in world
            % coordinates.
            if (SensorX<=0) | (SensorY<=0) | (SensorX>WorldDim) | (SensorY>WorldDim) continue; end;  % Don't return a reading from outside the world boundaries
            % If the cell is occupied, return a positive observation with
            % likelihood 0.6;  if unoccupied, return a positive observation
            % with likelihood 0.05.  Set to 1 and 0 if you want to debug.
            %if ((Occupied(SensorX,SensorY)==1) &(rand(1,1)<=TrueHit))
            %    Observation(i,j) = 1;
            %elseif (rand(1,1) <= FalseAlarm)  % Updated October 24, 2007
            %    Observation(i,j) = 1;
            %end;
            
            %%%%%%%%% UPDATED 10/9/09 Warren Lemmon %%%%%%%%%%%%
            if (Occupied(SensorX,SensorY)==1)
                if(rand(1,1)<=TrueHit)
                   Observation(i,j) = 1;
                end;
            end;
            if (Occupied(SensorX,SensorY)==0)
                if(rand(1,1) <= FalseAlarm)
                   Observation(i,j) = 1;
                end;
            end;
             
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % UPDATE PROBABILITIES USING BAYES RULE %
            % This is the grid filter.  For each    %
            % cell in the grid, update the probabi- %
            % lity of it be it be occupied using    %
            % Bayes rule given the observation.     %
            %                                       % 
            %                                       %
            % Updated October 24, 2007              %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	    % New, improved, and correct version
	    if (Observation(i,j) == 1)  % If we observe a hit
	        Bel_Occ = TrueHit * p(SensorX,SensorY);  % Recall that p(SensorX,SensorY) is the probability that a cell is occupied
            Bel_Unocc = FalseAlarm * (1-p(SensorX,SensorY));  % So 1-p(SensorX,SensorY) is the probability that a cell is unoccupied
	        % Normalize
            p(SensorX,SensorY) = Bel_Occ / (Bel_Occ + Bel_Unocc);
        else  % If do not observe a hit 
 	        Bel_Occ = (1-TrueHit) * p(SensorX,SensorY);  % Recall that p(SensorX,SensorY) is the probability that a cell is occupied
							     % Recall that (1-TrueHit) is the MissedDetection likelihood
            Bel_Unocc = (1-FalseAlarm) * (1-p(SensorX,SensorY));  % Recall 1-p(SensorX,SensorY) is the probability that a cell is unoccupied
							     % Recall that (1-FalseAlarm) is the TrueMiss likelihood
	        % Normalize
            p(SensorX,SensorY) = Bel_Occ / (Bel_Occ + Bel_Unocc);
               
        end;

            % Old version
	    %if (Observation(i,j) == 1)
            %    p(SensorX,SensorY) = TrueHit * p(SensorX,SensorY);
            %else
            %    p(SensorX,SensorY) = MissedDetection * p(SensorX,SensorY);
            %end;

	    % Update intensity to reflect probability that a cell is occupied. 
	    % Darker colors mean higher probabilities
            set(GridHandle(SensorX,SensorY),'color',[1-p(SensorX,SensorY),1-p(SensorX,SensorY),1-p(SensorX,SensorY)]);

        end;
    end;
    % Uncomment the following to see the sensor readings appear in figure 2.
    figure(2);
    mesh(Observation);
    title('Observation: 1 is hit, 0 is not hit');
    xlabel('x');ylabel('y');
    figure(1);
            
    %pause
    
end;
    
    

