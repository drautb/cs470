<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

CS 470 - Artificial Intelligence
================================

## Wednesday 11/6/13

* First-order Markov Process: $P(S_t|S_{t-1})$
* Second-order Markov Process: $P(S_t|S_{t-1}, S_{t-2})$

## Monday 11/4/13

* Bayes Filters combine bayes nets with bayes rule.
    * Filter as in _Noise Filter_. It filters out the noise in your measurements.

## Monday 10/28/13

* Bayes Nets:
    * Help us understand cause and effect relationships. 
    * Give us small joint probability tables, from which we can compute anything else we want to know.

* Canonical Nets
    * Noisy AND - There is a probability that even if all inputs are true, the output may be false.
        * Your car may have gas, electricity, and a starter, but there is still a chance that it won't start.
    * Noisy OR - 
        * Probability that I have a fever, given that I have the flu Or malaria OR a cold.
    * Naive Bayes - One cause, multiple effects
        * 

## Wednesday 10/23/13

The whole point of what we're doing is to avoid having to generate a complete conditional probability table. The Chain rule and properties of conditional independence greatly reduces what we need to represent to generate a joint probability distribution...I think.

## Monday 10/21/13

* Bayes rule is just a derivation of the product rule using the definitions of conditional and joint probability distributions.

* A random variable is a function

## Wednesday 10/9/13

### More Probability

_**Derived Distribution:**_ A distribution derived from our random variables.

_**Probability Mass Function:**_ Applie to discrete worlds. Definition:

$P_{X,Y}(a, b) = P( \\{ \omega: X(\omega) = a \wedge Y(\omega) = b \\} )$

This is a _Derived Distribution_.

$P_X(x)$ is a _Marginal Distribution_, $P_{X,Y}(x,y)$ is a _Joint Distribution_.

_**Marginalization:**_ The process of turning a _Joint Distribution_ into a _Marginal Distribution_. Defined by:

$P_X(x) = \sum_{Range of Y}^{} P_{X,Y}(x,y)$

_**Conditional Distribution:**_ Joint divided by Marginal. Defined by:

$P_{X|Y}(x|y) = \frac{P_{X,Y}(x,y)}{P_Y(y)}$

Another property of _Conditional Distributions_:

$\sum_{x}^{} P_{X|Y}(x|y) = 1$


### Bayes Rule

In this context, the random variables correspond to _State_ and _Observation_.

_**Bayes Rule:**_ $P_{S|O}(s|o) = \frac{P_{O|S}(o|s)P_S(s)}{P_O(o)}$

_**Posterior:**_ $P_{S|O}(s|o)$

_**Prior:**_ $P_S(s)$

_**Likelihood:**_ $P_{O|S}(o|s)$

_**Normalizer:**_ $P_O(o)$




## Monday 10/7/13

### Probability

_**Possible:**_ $\Omega$

_**Distinguishable:**_ $\sigma$

_**Size of distinguishable:**_ $P$

$P: \sigma \to [0,1]$ (Measures a set $\sigma$, and returns a unit of belief between 0 and 1)

$X: \Omega \to \Re$

$X: \Omega \to \\{ T, F \\}$

$P_X(x) = P( \\{ \omega:X(\omega)=x \\} )$

Ex: $P_X(horn) = P( \\{ \omega:X(\omega) = horn \\} )$

In book: P(Cavity=cavity)

* The uppercase C refers to the random variable, and the lowercase c refers to something from the Range of the random variable.


## Wednesday 10/2/13

### Genetic Algorithms

* Any time you do a mutation, you lose anything you may have learned about what happens after the point of mutation.
* Known as an _Anytime_ algorithm, because _anytime_ you stop the algorithm, it will give you the best chromosome it found 
so far.

### Gradient Descent

* $∇f = [\frac{\partial f}{\partial x}, \frac{\partial f}{\partial y}]$
* $\frac{\partial f}{\partial x}$ = Partial Derivative

$f(x,y) = 3xy^2 + 2x^2-xy+7+x$

$\frac{\partial f}{\partial x} = \frac{\partial[(3y^2)x+2x^2-(y)x+7+x]}{\partial x} = 3y^2+4x-y+1$

$\frac{\partial f}{\partial y} = \frac{\partial[(3x)y^2+2x^2-(x)y+7+x]}{\partial y} = 6xy-x$

$v = [0,1], k=0.2$

$v' = [0,1] + 0.2[3,0] = [0.6, 1]$


## Monday 9/30/13

Problem in class: When we have a search space where the probability of success on random-restart is $\frac{1}{4}$, how many times do we have to restart before we're 90% sure we've found the optimal solution?

$1 - (1-P_{success})^n > 0.9$

* Random restart performs well, but it can become very time consuming if the cost to compute the payoff (or cost) is high.
* Use Beam search when the cost to compute the payoff (or cost) is high. It focuses it's computational effort in areas where the probability of success is higher.

## Wednesday 9/25/13

* PEAS - Designer, CSA - Modeler
* Consequence = State X Action

* There are two kinds of models that we can choose:
    * Dr. G's Definition of Uncertainty: when we have to make a decision using some probabalistic model and we don't know everything about the environment, we call it decision making 
under uncertainty. If I can't see everything that's going on in the world, but I have a proabalistic model of whats going on, we call it decision making under uncertainty.
    * We may not always know the consequences that actions will produce, but if I have a probabalistic model for how my actions produce consequences, then we call it decision making in a nondeterministic world, or under nondeterminism.
    * It's the modeler's choice to decide which model. Where do you place the blame for uncertainty? (On not knowing the world? Or on not knowing what consequences will be produced?)



## Monday 9/16/13

* **Admissible Heuristic:** Never overestimates the true optimal cost from where I'm at to the goal.

**_Graph Search vs. Tree Search:_**
* _States_ are locations in a graph.
* _Nodes_ are a _state_, plus a parent _state_.
* Graph Searches have a _closed list_ of _nodes_.
* _Consistency_ guarantees that the first time you expand a node, you have found the optimal path to that node.
 

## Wednesday 9/11/13

**Search Presentations**
Differences between Greedy searches, textbook vs. CS 312
Differences betwee tree search and graph search


## Monday Sep. 9, 2013

*PD Controllers*
_Proportional Derivative_

$F = K_p * e + K_d \frac{de}{dt}$

> **Potential Fields Lab**

> Generated desired angle using artificial potential fields.

> Use PD controller to move from current angle to desired angle. The velocity of the tank is determined by the field.

