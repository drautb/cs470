package main

import (
	"bzrflag"
	"fmt"
	"math"
	"os"
	"strings"
	"tcpconn"
	"time"
)

const NUM_AGENTS int = 2

var conn tcpconn.TCPConn
var agentChannels []chan bzrflag.Tank

func main() {
	agentChannels = make([]chan bzrflag.Tank, NUM_AGENTS)
	for c := 0; c < NUM_AGENTS; c++ {
		agentChannels[c] = make(chan bzrflag.Tank)
	}

	var (
		host string
		port string
	)

	if len(os.Args) == 3 {
		host = os.Args[1]
		port = os.Args[2]
	} else {
		host = "localhost"
		port = "50100"
	}

	fmt.Printf("Starting Dumb Agent\n\tHost: %s\n\tPort: %s\n", host, port)
	if conn.Connect(host, port) == false {
		fmt.Println("\tFailed to connect!")
		os.Exit(1)
	} else {
		fmt.Println("\tConnection Established.")
	}

	fmt.Println("\tSending Handshake.\n")
	conn.Write("agent 1\n")
	conn.ReadLine()
	conn.UnLock()

	/**
	 * Start Agent stuff
	 */
	for a := 0; a < NUM_AGENTS; a++ {
		go RunDumbAgent(a)
	}
	UpdateTanks()

	/**
	 * End Agent Stuff
	 */

	fmt.Println("Terminating.")
	conn.Disconnect()
}

func UpdateTanks() {
	ticker := time.NewTicker(time.Millisecond * 500)
	for {
		<-ticker.C
		response := conn.SendMessage("mytanks")
		if response == false {
			return
		}
		lines := conn.ReadList()
		for i := 0; i < len(lines); i++ {
			if strings.Contains(lines[i], "mytank ") {
				t := bzrflag.BuildTankFromData(lines[i])
				if t.Index < NUM_AGENTS {
					(agentChannels[t.Index]) <- t
				}
			}
		}
	}
}

func RunDumbAgent(id int) {
	myTank := <-agentChannels[id]

	shotTicker := time.NewTicker(bzrflag.RandomDuration(1500, 2500))
	moveTicker := time.NewTicker(bzrflag.RandomDuration(3000, 8000))

	myTank.Move(conn, 0.5)
	lastAngle := myTank.Angle

	for {
		select {
		case myTank = <-agentChannels[id]:
			angleDiff := bzrflag.AngleDifference(lastAngle, myTank.Angle)
			if angleDiff > (math.Pi / 3.0) {
				myTank.Rotate(conn, 0.0)
				myTank.Move(conn, 0.5)
				lastAngle = myTank.Angle
				moveTicker = time.NewTicker(bzrflag.RandomDuration(3000, 8000))
			}
		case <-shotTicker.C:
			myTank.Shoot(conn)
			shotTicker = time.NewTicker(bzrflag.RandomDuration(1500, 2500))
		case <-moveTicker.C:
			myTank.Move(conn, 0.0)
			myTank.Rotate(conn, 0.6)
		}
	}
}
