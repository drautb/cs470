package tcpconn

import (
	"bufio"
	"fmt"
	"net"
	"strings"
	// "time"
)

type TCPConn struct {
	conn   net.Conn
	buffer *bufio.Reader
	Line   string
	e      error
	mutex  chan string
}

func init() {

}

func (tcpConn *TCPConn) Connect(host string, port string) bool {
	connStr := fmt.Sprint(host, ":", port)
	tcpConn.conn, tcpConn.e = net.Dial("tcp", connStr)

	if tcpConn.e != nil {
		if tcpConn.conn != nil {
			tcpConn.conn.Close()
		}
		return false
	}

	tcpConn.buffer = bufio.NewReader(tcpConn.conn)
	tcpConn.mutex = make(chan string, 1)
	tcpConn.mutex <- "Go!"

	return true
}

func (tcpConn *TCPConn) Disconnect() {
	if tcpConn.conn != nil {
		tcpConn.conn.Close()
	}
}

func (tcpConn *TCPConn) Lock() {
	<-tcpConn.mutex
}

func (tcpConn *TCPConn) UnLock() {
	tcpConn.mutex <- "done"
}

func (tcpConn *TCPConn) ReadLine() {
	tcpConn.Line, tcpConn.e = tcpConn.buffer.ReadString('\n')
	// fmt.Print("\tRead Line: ", tcpConn.Line)
}

func (tcpConn *TCPConn) ReadAck() bool {
	tcpConn.ReadLine()
	if strings.Contains(tcpConn.Line, "ack ") {
		return true
	}
	fmt.Println("Expected ack, found: ", tcpConn.Line)
	return false
}

func (tcpConn *TCPConn) ReadStatus() bool {
	tcpConn.ReadLine()
	tcpConn.UnLock()
	if strings.Contains(tcpConn.Line, "ok") || strings.Contains(tcpConn.Line, "fail") {
		return true
	}
	fmt.Println("Expected status, found: ", tcpConn.Line)
	return false
}

func (tcpConn *TCPConn) ReadBegin() bool {
	tcpConn.ReadLine()
	if strings.Contains(tcpConn.Line, "begin") {
		return true
	}
	fmt.Println("Expected begin, found: ", tcpConn.Line)
	return false
}

func (tcpConn *TCPConn) ReadEnd() bool {
	tcpConn.ReadLine()
	if strings.Contains(tcpConn.Line, "end") {
		return true
	}
	return false
}

func (tcpConn *TCPConn) ReadList() []string {
	var lines []string
	if tcpConn.ReadBegin() {
		for {
			if tcpConn.ReadEnd() == false {
				lines = append(lines, tcpConn.Line)
			} else {
				tcpConn.UnLock()
				break
			}
		}
	}
	return lines
}

func (tcpConn *TCPConn) Write(strToWrite string) {
	// fmt.Print("Writing: ", strToWrite)
	tcpConn.Lock()
	tcpConn.conn.Write([]byte(strToWrite))
}

func (tcpConn *TCPConn) SendMessage(msg string) bool {
	msg = fmt.Sprint(msg, "\n")
	tcpConn.Write(msg)
	// time.Sleep(time.Millisecond * 10)
	return tcpConn.ReadAck()
}
