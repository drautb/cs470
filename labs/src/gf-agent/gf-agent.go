package main

import (
	"bzrflag"
	"fmt"
	gl "github.com/go-gl/gl"
	glfw "github.com/go-gl/glfw3"
	"math"
	"math/rand"
	"os"
	"runtime"
	"strconv"
	"strings"
	"tcpconn"
	"time"
)

type goal struct {
	x float64
	y float64
}

type vector struct {
	angle     float64
	magnitude float64
}

const UPDATE_INTERVAL_MS = 50
const OBSTACLE_UPDATE_INTERVAL_MS = 300
const MAX_OBSTACLE_RADIUS = 50.0
const STUCK_DELAY = 2000

var conn tcpconn.TCPConn
var agentChannels []chan bzrflag.Tank

var NUM_AGENTS int

var WORLD_SIZE int
var HALF_WORLD_SIZE float64
var TRUE_HIT float64
var FALSE_ALARM float64
var OBSTACLE_GRANULARITY int
var OBSTACLE_SIZE float64
var OBSTACLE_THRESHOLD float64

var updateGrid chan int
var renderGrid chan string
var beliefGrid [][]float64

var obstacleIndex []int8
var obstacleList []bzrflag.Obstacle

var currentGoal []goal
var minGoal []goal

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	var (
		host string
		port string
	)

	if len(os.Args) == 6 {
		host = os.Args[1]
		port = os.Args[2]
		NUM_AGENTS, _ = strconv.Atoi(os.Args[3])
		OBSTACLE_GRANULARITY, _ = strconv.Atoi(os.Args[4])
		OBSTACLE_THRESHOLD, _ = strconv.ParseFloat(os.Args[5], 64)
	} else {
		host = "localhost"
		port = "50101"
		NUM_AGENTS = 4
		OBSTACLE_GRANULARITY = 8
	}

	agentChannels = make([]chan bzrflag.Tank, NUM_AGENTS)
	for c := 0; c < NUM_AGENTS; c++ {
		agentChannels[c] = make(chan bzrflag.Tank, 10)
	}

	obstacleList = make([]bzrflag.Obstacle, 0)
	obstacleIndex = make([]int8, OBSTACLE_GRANULARITY*OBSTACLE_GRANULARITY)
	currentGoal = make([]goal, NUM_AGENTS)
	minGoal = make([]goal, NUM_AGENTS)

	fmt.Printf("Starting Grid Filter Agent\n\tHost: %s\n\tPort: %s\n", host, port)
	if conn.Connect(host, port) == false {
		fmt.Println("\tFailed to connect!")
		os.Exit(1)
	} else {
		fmt.Println("\tConnection Established.")
	}

	fmt.Println("\tSending Handshake.\n")
	conn.Write("agent 1\n")
	conn.ReadLine()
	conn.UnLock()

	WORLD_SIZE = bzrflag.GetConstantInt(conn, "worldsize")
	HALF_WORLD_SIZE = float64(WORLD_SIZE / 2)
	OBSTACLE_SIZE = float64(WORLD_SIZE / OBSTACLE_GRANULARITY)
	TRUE_HIT = bzrflag.GetConstantFloat(conn, "truepositive")
	FALSE_ALARM = 1 - bzrflag.GetConstantFloat(conn, "truenegative")
	updateGrid = make(chan int, 20)
	renderGrid = make(chan string, 100)
	beliefGrid = make([][]float64, WORLD_SIZE)
	for i := range beliefGrid {
		beliefGrid[i] = make([]float64, WORLD_SIZE)
	}

	// Initialize beliefGrid to be inconclusive (50% that any cell is occupied)
	for col := 0; col < WORLD_SIZE; col++ {
		for row := 0; row < WORLD_SIZE; row++ {
			beliefGrid[col][row] = 0.5
		}
	}

	/**
	 * Start Agent stuff
	 */
	go UpdateBeliefGrid()
	time.Sleep(50 * time.Millisecond)

	for t := 0; t < NUM_AGENTS; t++ {
		time.Sleep(50 * time.Millisecond)
		go RunGFAgent(t)
	}

	go ShowGridFilter()
	renderGrid <- "Go!"
	renderGrid <- "Go!"
	renderGrid <- "Go!"

	time.Sleep(50 * time.Millisecond)
	go RunObstacleDetector()

	time.Sleep(50 * time.Millisecond)
	UpdateTanks()

	/**
	 * End Agent Stuff
	 */

	fmt.Println("Terminating.")
	conn.Disconnect()
}

func RunObstacleDetector() {
	ticker := time.NewTicker(time.Millisecond * OBSTACLE_UPDATE_INTERVAL_MS)
	currentCell := -1
	cellCount := len(obstacleIndex)
	for {
		<-ticker.C
		for n := 0; n < OBSTACLE_GRANULARITY; n++ {

			currentCell++
			if currentCell >= cellCount {
				currentCell = 0
			}

			// Ok, in the current cell, we need to see if the majority of it is an obstacle.
			// if it is, then create an obstacle with a radius a little bigger than a cell size
			if obstacleIndex[currentCell] == 1 {
				continue
			}

			x, y, avg := GetAverageBelief(currentCell)
			if avg > OBSTACLE_THRESHOLD {
				var o bzrflag.Obstacle
				o.Cx = float64(x) + (OBSTACLE_SIZE / 2.0)
				o.Cy = float64(y) + (OBSTACLE_SIZE / 2.0)
				o.R = OBSTACLE_SIZE / 1.5
				obstacleList = append(obstacleList, o)
				obstacleIndex[currentCell] = 1
			}
		}
		go ConsolidateObstacles()
	}
}

func ConsolidateObstacles() {
	if len(obstacleList) < 2 {
		return
	}

	for {
		if !RemoveInsideObstacles() {
			break
		}
	}

	for o := 0; o < len(obstacleList)-1; o++ {
		for o2 := o + 1; o2 < len(obstacleList); o2++ {
			if obstacleList[o].Overlaps(&obstacleList[o2]) &&
				obstacleList[o].R == obstacleList[o2].R &&
				obstacleList[o].R != MAX_OBSTACLE_RADIUS {
				newObs := obstacleList[o].Consolidate(&obstacleList[o2], MAX_OBSTACLE_RADIUS)
				obstacleList = append(obstacleList, newObs)

				newObsList := make([]bzrflag.Obstacle, len(obstacleList)-2)
				copy(newObsList, obstacleList[:o])
				copy(newObsList[o:], obstacleList[o+1:o2])
				copy(newObsList[o2-1:], obstacleList[o2+1:])

				obstacleList = newObsList

				goto END_CONSOLIDATION
			}
		}
	}
END_CONSOLIDATION:
}

// Returns true if an obstacle was removed, false otherwise
func RemoveInsideObstacles() bool {
	for o := 0; o < len(obstacleList); o++ {
		for o2 := 0; o2 < len(obstacleList); o2++ {
			if o == o2 {
				continue
			}

			if obstacleList[o].Contains(&obstacleList[o2]) {
				newObsList := make([]bzrflag.Obstacle, len(obstacleList)-1)
				copy(newObsList, obstacleList[:o2])
				copy(newObsList[o2:], obstacleList[o2+1:])

				obstacleList = newObsList

				return true
			}
		}
	}

	return false
}

func GetAverageBelief(cellN int) (int, int, float64) {
	total := 0.0
	startX := (cellN % OBSTACLE_GRANULARITY) * int(OBSTACLE_SIZE)
	startY := (int(cellN / OBSTACLE_GRANULARITY)) * int(OBSTACLE_SIZE)
	endX := startX + int(OBSTACLE_SIZE)
	endY := startY + int(OBSTACLE_SIZE)
	for x := startX; x < endX; x++ {
		for y := startY; y < endY; y++ {
			total += beliefGrid[x][y]
		}
	}
	total /= (OBSTACLE_SIZE * OBSTACLE_SIZE)

	if total > 0.95 && total != 1.0 {
		for x := startX; x < endX; x++ {
			for y := startY; y < endY; y++ {
				beliefGrid[x][y] = 1.0
			}
		}
	} else if total < 0.05 && total != 0.0 {
		for x := startX; x < endX; x++ {
			for y := startY; y < endY; y++ {
				beliefGrid[x][y] = 0.0
			}
		}
	}

	return startX - int(HALF_WORLD_SIZE), startY - int(HALF_WORLD_SIZE), total
}

func UpdateTanks() {
	ticker := time.NewTicker(time.Millisecond * UPDATE_INTERVAL_MS)
	for {
		<-ticker.C
		response := conn.SendMessage("mytanks")
		if response == false {
			return
		}
		lines := conn.ReadList()
		for i := 0; i < len(lines); i++ {
			if strings.Contains(lines[i], "mytank ") {
				t := bzrflag.BuildTankFromData(lines[i])
				if t.Index < NUM_AGENTS {
					(agentChannels[t.Index]) <- t
				}
			}
		}
	}
}

func RunGFAgent(id int) {
	myTank := <-agentChannels[id]

	// shotTicker := time.NewTicker(bzrflag.RandomDuration(1500, 2500))
	// goalTicker := time.NewTicker(bzrflag.RandomDuration(, 2500))
	stuckTicker := time.NewTicker(time.Millisecond * STUCK_DELAY)
	lastX := 0.0
	lastY := 0.0

	if id == 0 {
		minGoal[id].x = -HALF_WORLD_SIZE
		minGoal[id].y = 0
	} else if id == 1 {
		minGoal[id].x = 0
		minGoal[id].y = 0
	} else if id == 2 {
		minGoal[id].x = -HALF_WORLD_SIZE
		minGoal[id].y = -HALF_WORLD_SIZE
	} else if id == 3 {
		minGoal[id].x = 0
		minGoal[id].y = -HALF_WORLD_SIZE
	}
	currentGoal[id].x = minGoal[id].x
	currentGoal[id].y = minGoal[id].y

	done := false

	for {
		select {
		case <-stuckTicker.C:
			currentGoal[id].x, currentGoal[id].y = FindUnexploredPixel()
			fmt.Printf("Reseting Tank #%d\n", id)
			stuckTicker = time.NewTicker(time.Millisecond * 10000) // Ten seconds to adjust to new point

		case myTank = <-agentChannels[id]:
			attempts := 0
			cellX := 0
			cellY := 0
			deltaX := 1
			deltaY := 1

			if math.Sqrt(math.Pow(myTank.X-lastX, 2)+math.Pow(myTank.Y-lastY, 2)) > 5.0 {
				lastX = myTank.X
				lastY = myTank.Y
				stuckTicker = time.NewTicker(time.Millisecond * STUCK_DELAY)
			}

			for {
				if done {
					if currentGoal[id].x == 0 && currentGoal[id].y == 0 {
						currentGoal[id].x, currentGoal[id].y = FindUnexploredPixel()
					}
					break
				}
				if attempts > (OBSTACLE_GRANULARITY*OBSTACLE_GRANULARITY/NUM_AGENTS)-1 {
					myTank.Move(conn, 0.0)
					myTank.Rotate(conn, 0.0)
					currentGoal[id].x = 0
					currentGoal[id].y = 0
					done = true
				} else if beliefGrid[int(currentGoal[id].x+HALF_WORLD_SIZE)][int(currentGoal[id].y+HALF_WORLD_SIZE)] != 0.5 {
					cellX += deltaX
					if cellX > OBSTACLE_GRANULARITY/2-1 || cellX < 0 {
						deltaX *= -1
						cellX += deltaX
						cellY += deltaY
						if cellY > OBSTACLE_GRANULARITY/2-1 || cellY < 0 {
							deltaY *= -1
							cellY += deltaY
						}
					}
					currentGoal[id].x = float64(minGoal[id].x + float64(cellX)*float64(OBSTACLE_SIZE) + OBSTACLE_SIZE/2.0)
					currentGoal[id].y = float64(minGoal[id].y + float64(cellY)*float64(OBSTACLE_SIZE) + OBSTACLE_SIZE/2.0)
					attempts += 1
				} else {
					break
				}
			}

			v := GetPFVector(myTank, currentGoal[id], obstacleList)
			angVel := GetAngVel(v.angle, myTank.Angle)
			// Add some noise to their angVel.
			angVel += rand.Float64() - 0.5
			myTank.Rotate(conn, angVel)
			myTank.Move(conn, v.magnitude)
		}
	}
}

// func GetNeighborsGoal(myId int) (float64, float64) {
// 	curId := myId
// 	for {
// 		curId++
// 		if curId >= NUM_AGENTS {
// 			curId = 0
// 		}
// 		if curId == myId {
// 			break
// 		}

// 		if currentGoal[curId].x != 0 && currentGoal[curId].y != 0 {
// 			return currentGoal[curId].x, currentGoal[curId].y
// 		}
// 	}

// 	return 0, 0
// }

func UpdateBeliefGrid() {
	var tankId int = 0
	ticker := time.NewTicker(time.Millisecond * 100)
	for {
		// Wait for data
		<-ticker.C
		tankId += 1
		if tankId >= NUM_AGENTS {
			tankId = 0
		}

		cmd := fmt.Sprintf("occgrid %d", tankId)
		response := conn.SendMessage(cmd)
		if !response {
			panic("Failed to get occgrid response!")
		}
		responseList := conn.ReadList()

		// Position Values
		xStr := responseList[0][3:strings.Index(responseList[0], ",")]
		yStr := responseList[0][(strings.Index(responseList[0], ",") + 1):(len(responseList[0]) - 1)]
		startX, err := strconv.Atoi(xStr)
		startY, err := strconv.Atoi(yStr)
		if err != nil {
			panic(err)
		}

		// fmt.Printf("X,Y Start: %d, %d\n", startX, startY)

		// Dimensions
		// wStr := responseList[1][5:strings.Index(responseList[1], "x")]
		hStr := responseList[1][(strings.Index(responseList[1], "x") + 1):(len(responseList[1]) - 1)]
		// widthX, err := strconv.Atoi(wStr)
		widthY, err := strconv.Atoi(hStr)
		if err != nil {
			panic(err)
		}

		// fmt.Printf("Width,Height: %d, %d\n", widthX, widthY)

		// Ok, so the first "row" that the sensor returns goes from (x,y+height) to (x,y)
		// 													Row 2:  (x+1, y+height) to (x+1, y)
		for row := 2; row < len(responseList); row++ {
			data := responseList[row]
			xIdx := row - 2
			x := startX + (WORLD_SIZE / 2.0) + xIdx
			for yIdx := 0; yIdx < widthY; yIdx++ {
				y := startY + (WORLD_SIZE / 2.0) + yIdx
				if strings.EqualFold(string(data[yIdx]), "1") {
					beliefOcc := TRUE_HIT * beliefGrid[x][y]
					beliefUnocc := FALSE_ALARM * (1 - beliefGrid[x][y])
					beliefGrid[x][y] = beliefOcc / (beliefOcc + beliefUnocc)
				} else {
					beliefOcc := (1 - TRUE_HIT) * beliefGrid[x][y]
					beliefUnocc := (1 - FALSE_ALARM) * (1 - beliefGrid[x][y])
					beliefGrid[x][y] = beliefOcc / (beliefOcc + beliefUnocc)
				}
			}
		}
		renderGrid <- "GO!"
		renderGrid <- "GO!"
	}
}

func GetPFVector(t bzrflag.Tank, g goal, oList []bzrflag.Obstacle) vector {
	ALPHA := 0.01 // Attractive Scalar
	BETA := 0.06  // Repulsive Scalar
	GAMMA := 0.03 // Tangential Scalar

	REPUSLIVE_S := 20.0
	TANGENTIAL_S := REPUSLIVE_S + 20.0

	MIN_CLAMP := -1.0
	MAX_CLAMP := 1.0

	diffX := g.x - t.X
	diffY := g.y - t.Y
	distance := bzrflag.Distance(g.x, g.y, t.X, t.Y)
	theta := math.Atan2(diffY, diffX)

	dX := bzrflag.Clamp(ALPHA*distance*math.Cos(theta), MIN_CLAMP, MAX_CLAMP)
	dY := bzrflag.Clamp(ALPHA*distance*math.Sin(theta), MIN_CLAMP, MAX_CLAMP)

	for oIdx := 0; oIdx < len(oList); oIdx++ {
		o := oList[oIdx]

		diffX = o.Cx - t.X
		diffY = o.Cy - t.Y
		distance = bzrflag.Distance(o.Cx, o.Cy, t.X, t.Y)
		theta = math.Atan2(diffY, diffX)

		// Repulsive
		if distance < (o.R + REPUSLIVE_S) {
			dX -= bzrflag.Clamp(BETA*(REPUSLIVE_S+o.R-distance)*math.Cos(theta), MIN_CLAMP, MAX_CLAMP)
			dY -= bzrflag.Clamp(BETA*(REPUSLIVE_S+o.R-distance)*math.Sin(theta), MIN_CLAMP, MAX_CLAMP)
		}

		// Tangential
		if distance < (o.R + TANGENTIAL_S) {
			obsVX := o.Cx - g.x
			obsVY := o.Cy - g.y
			tankVX := t.X - g.x
			tankVY := t.Y - g.y
			dot := obsVX*tankVY - obsVY*tankVX
			var rotatedTheta float64 = math.Pi / 2.0

			// If we don't do this to make the tangential field change direction, then tanks
			// will still get stuck going in circles.
			if dot < 0 {
				rotatedTheta *= -1
			}

			dX -= bzrflag.Clamp(GAMMA*(TANGENTIAL_S+o.R-distance)*math.Cos(theta+rotatedTheta), MIN_CLAMP, MAX_CLAMP)
			dY -= bzrflag.Clamp(GAMMA*(TANGENTIAL_S+o.R-distance)*math.Sin(theta+rotatedTheta), MIN_CLAMP, MAX_CLAMP)
		}
	}

	var v vector
	v.angle = math.Atan2(dY, dX)
	v.magnitude = math.Sqrt(math.Pow(dX, 2) + math.Pow(dY, 2))
	if v.magnitude > 0 {
		v.magnitude = math.Max(v.magnitude, 0.5)
	}

	return v
}

const KP float64 = 1.0
const KD float64 = 1.0

var lastError float64 = 0.0

func GetAngVel(desiredAngle float64, currentAngle float64) float64 {
	angError := bzrflag.AngleDifference(desiredAngle, currentAngle)
	deltaError := (angError - lastError) / UPDATE_INTERVAL_MS
	angVel := KP*angError + KD*deltaError
	lastError = angError
	return angVel
}

/***************************************************
 * Functions for rendering the belief grid window
 ***************************************************/

func glfwErrorCallback(err glfw.ErrorCode, desc string) {
	fmt.Printf("%v: %v\n", err, desc)
}

func glPlotCircle(x float64, y float64, r float64) {
	var i int
	var lineAmount int = 100 //# of triangles used to draw circle

	twicePi := 2.0 * math.Pi

	gl.Begin(gl.LINE_LOOP)
	for i = 0; i <= lineAmount; i++ {
		gl.Vertex2d(
			x+(r*math.Cos(float64(i)*twicePi/float64(lineAmount))),
			y+(r*math.Sin(float64(i)*twicePi/float64(lineAmount))))
	}
	gl.End()
}

func ShowGridFilter() {
	glfw.SetErrorCallback(glfwErrorCallback)

	if !glfw.Init() {
		panic("Failed to initialize GLFW")
	}
	defer glfw.Terminate()

	window, err := glfw.CreateWindow(WORLD_SIZE, WORLD_SIZE, "Grid Filter", nil, nil)
	if err != nil {
		panic(err)
	}

	window.MakeContextCurrent()

	gl.MatrixMode(gl.PROJECTION)
	gl.LoadIdentity()
	gl.Ortho(0, float64(WORLD_SIZE), 0, float64(WORLD_SIZE), 0, 1)
	gl.MatrixMode(gl.MODELVIEW)
	gl.Disable(gl.DEPTH_TEST)
	gl.ClearColor(0.0, 1.0, 0.0, 1.0)

	for !window.ShouldClose() {
		// Wait for a signal to update the drawing
		<-renderGrid

		gl.Clear(gl.COLOR_BUFFER_BIT)

		gl.Begin(gl.POINTS)
		for col := 0; col < WORLD_SIZE; col++ {
			for row := 0; row < WORLD_SIZE; row++ {
				gl.Color3f(1.0-float32(beliefGrid[col][row]),
					1.0-float32(beliefGrid[col][row]),
					1.0-float32(beliefGrid[col][row]))
				gl.Vertex2i(col, row)
			}
		}

		gl.End()

		gl.Color3f(1.0, 0.0, 0.0)

		gl.Begin(gl.QUADS)
		for n := 0; n < NUM_AGENTS; n++ {
			gl.Vertex2d(currentGoal[n].x-5.0+HALF_WORLD_SIZE, currentGoal[n].y-5.0+HALF_WORLD_SIZE)
			gl.Vertex2d(currentGoal[n].x+5.0+HALF_WORLD_SIZE, currentGoal[n].y-5.0+HALF_WORLD_SIZE)
			gl.Vertex2d(currentGoal[n].x+5.0+HALF_WORLD_SIZE, currentGoal[n].y+5.0+HALF_WORLD_SIZE)
			gl.Vertex2d(currentGoal[n].x-5.0+HALF_WORLD_SIZE, currentGoal[n].y+5.0+HALF_WORLD_SIZE)
		}
		gl.End()

		// gl.Begin(gl.LINES)
		// for r := 0; r < OBSTACLE_GRANULARITY; r++ {
		// 	gl.Vertex2i(0, r*int(OBSTACLE_SIZE))
		// 	gl.Vertex2i(WORLD_SIZE, r*int(OBSTACLE_SIZE))
		// }
		// for c := 0; c < OBSTACLE_GRANULARITY; c++ {
		// 	gl.Vertex2i(c*int(OBSTACLE_SIZE), 0)
		// 	gl.Vertex2i(c*int(OBSTACLE_SIZE), WORLD_SIZE)
		// }
		// gl.End()

		gl.Color3f(0.0, 0.0, 1.0)
		for o := 0; o < len(obstacleList); o++ {
			glPlotCircle(obstacleList[o].Cx+HALF_WORLD_SIZE,
				obstacleList[o].Cy+HALF_WORLD_SIZE,
				obstacleList[o].R)
		}

		window.SwapBuffers()
		glfw.PollEvents()
	}
}

func FindUnexploredPixel() (float64, float64) {
	startX := rand.Intn(WORLD_SIZE)
	startY := rand.Intn(WORLD_SIZE)

	fmt.Println("\nEntering unexp loop...")
	for x := startX; x < WORLD_SIZE; x++ {
		for y := startY; y < WORLD_SIZE; y++ {
			if beliefGrid[x][y] > 0.3 && beliefGrid[x][y] < 0.7 {
				fmt.Println("\tReturning!")
				return float64(x) - HALF_WORLD_SIZE, float64(y) - HALF_WORLD_SIZE
			}
		}
	}

	return 0, 0
}
