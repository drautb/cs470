package main

import (
	"bzrflag"
	"fmt"
	"math"
	"os"
	"strings"
	"tcpconn"
	"time"
)

type goal struct {
	x float64
	y float64
}

type vector struct {
	angle     float64
	magnitude float64
}

const NUM_AGENTS int = 1
const UPDATE_INTERVAL_MS = 100

var conn tcpconn.TCPConn
var agentChannels []chan bzrflag.Tank

var obstacleList []bzrflag.Obstacle
var baseList []bzrflag.Base

func main() {
	agentChannels = make([]chan bzrflag.Tank, NUM_AGENTS)
	for c := 0; c < NUM_AGENTS; c++ {
		agentChannels[c] = make(chan bzrflag.Tank)
	}
	// mainChannel = make(chan string)

	var (
		host string
		port string
		flag string
	)

	if len(os.Args) == 4 {
		host = os.Args[1]
		port = os.Args[2]
		flag = os.Args[3]
	} else {
		host = "localhost"
		port = "50101"
		flag = "blue"
	}

	fmt.Printf("Starting Dumb Agent\n\tHost: %s\n\tPort: %s\n", host, port)
	if conn.Connect(host, port) == false {
		fmt.Println("\tFailed to connect!")
		os.Exit(1)
	} else {
		fmt.Println("\tConnection Established.")
	}

	fmt.Println("\tSending Handshake.\n")
	conn.Write("agent 1\n")
	conn.ReadLine()
	conn.UnLock()

	/**
	 * Start Agent stuff
	 */
	obstacleList = bzrflag.GetAllObstacles(conn)
	baseList = bzrflag.GetAllBases(conn)

	go RunPFAgent(0, flag)

	UpdateTanks()

	/**
	 * End Agent Stuff
	 */

	fmt.Println("Terminating.")
	conn.Disconnect()
}

func UpdateTanks() {
	ticker := time.NewTicker(time.Millisecond * UPDATE_INTERVAL_MS)
	for {
		<-ticker.C
		response := conn.SendMessage("mytanks")
		if response == false {
			return
		}
		lines := conn.ReadList()
		for i := 0; i < len(lines); i++ {
			if strings.Contains(lines[i], "mytank ") {
				t := bzrflag.BuildTankFromData(lines[i])
				if t.Index < NUM_AGENTS {
					(agentChannels[t.Index]) <- t
				}
			}
		}
	}
}

func RunPFAgent(id int, target string) {
	myTank := <-agentChannels[id]
	shotTicker := time.NewTicker(bzrflag.RandomDuration(1500, 2500))
	var currentGoal, baseGoal, flagGoal goal

	myBase := bzrflag.GetBaseFromList(baseList, myTank.Team)
	baseGoal.x = myBase.Cx
	baseGoal.y = myBase.Cy

	for {
		select {
		case myTank = <-agentChannels[id]:
			if myTank.HasFlag != "-" {
				currentGoal = baseGoal
			} else {
				f := bzrflag.GetFlag(conn, target)
				flagGoal.x = f.X
				flagGoal.y = f.Y
				currentGoal = flagGoal
			}
			v := GetPFVector(myTank, currentGoal, obstacleList)
			angVel := GetAngVel(v.angle, myTank.Angle)
			myTank.Rotate(conn, angVel)
			myTank.Move(conn, v.magnitude-angVel)
		case <-shotTicker.C:
			myTank.Shoot(conn)
			shotTicker = time.NewTicker(bzrflag.RandomDuration(1500, 2500))
		}
	}
}

func GetPFVector(t bzrflag.Tank, g goal, oList []bzrflag.Obstacle) vector {
	ALPHA := 0.01 // Attractive Scalar
	BETA := 0.06  // Repulsive Scalar
	GAMMA := 0.03 // Tangential Scalar

	REPUSLIVE_S := 20.0
	TANGENTIAL_S := REPUSLIVE_S + 20.0

	MIN_CLAMP := -1.0
	MAX_CLAMP := 1.0

	diffX := g.x - t.X
	diffY := g.y - t.Y
	distance := bzrflag.Distance(g.x, g.y, t.X, t.Y)
	theta := math.Atan2(diffY, diffX)

	dX := bzrflag.Clamp(ALPHA*distance*math.Cos(theta), MIN_CLAMP, MAX_CLAMP)
	dY := bzrflag.Clamp(ALPHA*distance*math.Sin(theta), MIN_CLAMP, MAX_CLAMP)

	for oIdx := 0; oIdx < len(oList); oIdx++ {
		o := oList[oIdx]

		diffX = o.Cx - t.X
		diffY = o.Cy - t.Y
		distance = bzrflag.Distance(o.Cx, o.Cy, t.X, t.Y)
		theta = math.Atan2(diffY, diffX)

		// Repulsive
		if distance < (o.R + REPUSLIVE_S) {
			dX -= bzrflag.Clamp(BETA*(REPUSLIVE_S+o.R-distance)*math.Cos(theta), MIN_CLAMP, MAX_CLAMP)
			dY -= bzrflag.Clamp(BETA*(REPUSLIVE_S+o.R-distance)*math.Sin(theta), MIN_CLAMP, MAX_CLAMP)
		}

		// Tangential
		if distance < (o.R + TANGENTIAL_S) {
			obsVX := o.Cx - g.x
			obsVY := o.Cy - g.y
			tankVX := t.X - g.x
			tankVY := t.Y - g.y
			dot := obsVX*tankVY - obsVY*tankVX
			var rotatedTheta float64 = math.Pi / 2.0
			if dot < 0 {
				rotatedTheta *= -1
			}

			dX -= bzrflag.Clamp(GAMMA*(TANGENTIAL_S+o.R-distance)*math.Cos(theta+rotatedTheta), MIN_CLAMP, MAX_CLAMP)
			dY -= bzrflag.Clamp(GAMMA*(TANGENTIAL_S+o.R-distance)*math.Sin(theta+rotatedTheta), MIN_CLAMP, MAX_CLAMP)
		}
	}

	var v vector
	v.angle = math.Atan2(dY, dX)
	v.magnitude = math.Sqrt(math.Pow(dX, 2) + math.Pow(dY, 2))
	if v.magnitude > 0 {
		v.magnitude = math.Max(v.magnitude, 0.5)
	}

	return v
}

const KP float64 = 1.0
const KD float64 = 1.0

var lastError float64 = 0.0

func GetAngVel(desiredAngle float64, currentAngle float64) float64 {
	angError := bzrflag.AngleDifference(desiredAngle, currentAngle)
	deltaError := (angError - lastError) / UPDATE_INTERVAL_MS
	angVel := KP*angError + KD*deltaError
	lastError = angError
	return angVel
}
