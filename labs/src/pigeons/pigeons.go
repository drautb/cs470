package main

import (
	"bzrflag"
	"fmt"
	// "math"
	// "math/rand"
	"os"
	"strings"
	"tcpconn"
	"time"
)

const NUM_AGENTS int = 1

const UPDATE_INTERVAL = 50

var conn tcpconn.TCPConn
var agentChannels []chan bzrflag.Tank

func main() {
	agentChannels = make([]chan bzrflag.Tank, NUM_AGENTS)
	for c := 0; c < NUM_AGENTS; c++ {
		agentChannels[c] = make(chan bzrflag.Tank)
	}

	var (
		host   string
		port   string
		pigeon string
	)

	if len(os.Args) == 4 {
		host = os.Args[1]
		port = os.Args[2]
		pigeon = os.Args[3]
	} else {
		host = "localhost"
		port = "50101"
		pigeon = "lump"
	}

	fmt.Printf("Starting Dumb Agent\n\tHost: %s\n\tPort: %s\n", host, port)
	if conn.Connect(host, port) == false {
		fmt.Println("\tFailed to connect!")
		os.Exit(1)
	} else {
		fmt.Println("\tConnection Established.")
	}

	fmt.Println("\tSending Handshake.\n")
	conn.Write("agent 1\n")
	conn.ReadLine()
	conn.UnLock()

	/**
	 * Start Agent stuff
	 */
	switch pigeon {
	case "lump":
		go RunLumpy(0)
	case "beeline":
		go RunBeeliner(0)
	case "wild":
		go RunCrayCray(0)
	default:
		go RunLumpy(0)
	}
	UpdateTanks()

	/**
	 * End Agent Stuff
	 */

	fmt.Println("Terminating.")
	conn.Disconnect()
}

func UpdateTanks() {
	ticker := time.NewTicker(time.Millisecond * UPDATE_INTERVAL)
	for {
		<-ticker.C
		response := conn.SendMessage("mytanks")
		if response == false {
			return
		}
		lines := conn.ReadList()
		for i := 0; i < len(lines); i++ {
			if strings.Contains(lines[i], "mytank ") {
				t := bzrflag.BuildTankFromData(lines[i])
				if t.Index < NUM_AGENTS {
					(agentChannels[t.Index]) <- t
				}
			}
		}
	}
}

func RunBeeliner(id int) {
	myTank := <-agentChannels[id]

	for {
		select {
		case myTank = <-agentChannels[id]:
			if (myTank.Angle > 2.8 && myTank.Angle < 3.14) ||
				(myTank.Angle > -3.14 && myTank.Angle < -2.8) {
				myTank.Rotate(conn, 0.0)
				goto CONTINUE
			} else {
				myTank.Rotate(conn, 0.25)
			}
		}
	}

CONTINUE:

	for {
		select {
		case myTank = <-agentChannels[id]:
			myTank.Move(conn, 1.0)
		}
	}
}

func RunLumpy(id int) {
	<-agentChannels[id]

	for {
		select {
		case <-agentChannels[id]:
			// This guy sits there like a lump
		}
	}
}

/**
 * CrayCray basically drives in a big circle, but at varying speeds.
 * Circular motion isn't modeled by the basic physics equations, so this
 * should trip up the filter quite a bit. Add some speed variance, and he
 * should be hard to hit.
 */
func RunCrayCray(id int) {
	myTank := <-agentChannels[id]

	// lastAngVel := 0.0
	// lastSpeed := 0.0
	for {
		select {
		case myTank = <-agentChannels[id]:
			if myTank.Angle > 1.5 && myTank.Angle < 1.6 {
				myTank.Rotate(conn, 0.0)
				goto CONTINUE
			} else {
				myTank.Rotate(conn, 0.25)
			}
		}
	}

CONTINUE:

	for {
		select {
		case myTank = <-agentChannels[id]:
			// lastAngVel += rand.Float64()/10.0 - 0.05
			// lastAngVel = math.Min(0.6, lastAngVel)
			// lastAngVel = math.Max(0.2, lastAngVel)
			// lastSpeed += rand.Float64()/10.0 - 0.05
			// lastSpeed = math.Max(0.5, las)
			// lastSpeed = math.Min(lastSpeed, 1.0)
			myTank.Move(conn, 1.0)
			myTank.Rotate(conn, 0.3)
		}
	}
}
