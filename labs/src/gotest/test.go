package main

import (
    matrix "code.google.com/p/biogo.matrix"
    "fmt"
    // "math"
)

func main() {

    A := matrix.Must(matrix.NewDense(
        [][]float64{
            {1, 2, 3},
            {4, 5, 6},
        }))

    B := matrix.Must(matrix.NewDense(
        [][]float64{
            {2, 3, 4},
            {5, 6, 7},
        }))

    fmt.Println(A.T(nil))

    fmt.Println(A)
    fmt.Println(B)
    fmt.Println(B.Sub(A, nil))
    fmt.Println(A)
    fmt.Println(B)

}
