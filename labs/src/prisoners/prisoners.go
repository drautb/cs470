package main

import (
	"fmt"
	"math/rand"
)

const ITERATIONS int = 1000000

func main() {
	fmt.Printf("Performing Prisoner Analysis with %d iterations.\n", ITERATIONS)

	// These values are only for when the message is delivered to B
	aExecuted := 0.0
	bExecuted := 0.0
	cExecuted := 0.0

	for i := 0; i < ITERATIONS; i++ {
		execute := rand.Float64()

		if execute < (1.0 / 3.0) { // A is being executed
			message := rand.Float64()
			if message < 0.50 { // B is given the message
				aExecuted++
			}

		} else if execute < (2.0 / 3.0) { // B is being executed
			// So C is given the message, so we don't do anything
		} else { // C is being executed
			// B is given the message
			cExecuted++
		}
	}

	bGivenMessage := aExecuted + cExecuted

	fmt.Println("Anaylsis Complete!")
	fmt.Println("\nResults:")
	fmt.Printf("B was given the pardon message %d times.\n\n", int(bGivenMessage))
	fmt.Printf("A was executed %d times.\n", int(aExecuted))
	fmt.Printf("B was executed %d times.\n", int(bExecuted))
	fmt.Printf("C was executed %d times.\n", int(cExecuted))

	fmt.Println()
	fmt.Printf("A Execution Percentage: %d%%\n", int((aExecuted/bGivenMessage)*100))
	fmt.Printf("B Execution Percentage: %d%%\n", int((bExecuted/bGivenMessage)*100))
	fmt.Printf("C Execution Percentage: %d%%\n", int((cExecuted/bGivenMessage)*100))

}
