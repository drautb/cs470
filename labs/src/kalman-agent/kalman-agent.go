package main

import (
    "bzrflag"
    matrix "code.google.com/p/biogo.matrix"
    "fmt"
    gl "github.com/go-gl/gl"
    glfw "github.com/go-gl/glfw3"
    "math"
    "os"
    "runtime"
    "strings"
    "tcpconn"
    "time"
)

type goal struct {
    x   float64
    y   float64
}

type vector struct {
    angle     float64
    magnitude float64
}

const UPDATE_INTERVAL_MS = 75
const DT_S = 0.2
const C = 0.0

var conn tcpconn.TCPConn
var agentChannels []chan bzrflag.Tank

var NUM_AGENTS int

var WORLD_SIZE int
var HALF_WORLD_SIZE float64

var obstacleList []bzrflag.Obstacle

var currentGoal []goal

var renderGrid chan string

var eTank bzrflag.Tank

var F matrix.Matrix
var SigX matrix.Matrix
var H matrix.Matrix
var SigZ matrix.Matrix
var FT matrix.Matrix
var HT matrix.Matrix
var MuT matrix.Matrix
var SigT matrix.Matrix
var ZT *matrix.Dense

var predMuT matrix.Matrix

func main() {
    runtime.GOMAXPROCS(runtime.NumCPU())

    F = matrix.Must(matrix.NewDense(
        [][]float64{
            {1, DT_S, math.Pow(DT_S, 2) / 2.0, 0, 0, 0},
            {0, 1, DT_S, 0, 0, 0},
            {0, -C, 1, 0, 0, 0},
            {0, 0, 0, 1, DT_S, math.Pow(DT_S, 2) / 2.0},
            {0, 0, 0, 0, 1, DT_S},
            {0, 0, 0, 0, -C, 1},
        }))

    SigX = matrix.Must(matrix.NewDense(
        [][]float64{
            {0.01, 0, 0, 0, 0, 0},
            {0, 0.1, 0, 0, 0, 0},
            {0, 0, 5, 0, 0, 0},
            {0, 0, 0, 0.01, 0, 0},
            {0, 0, 0, 0, 0.1, 0},
            {0, 0, 0, 0, 0, 5},
        }))

    H = matrix.Must(matrix.NewDense(
        [][]float64{
            {1, 0, 0, 0, 0, 0},
            {0, 0, 0, 1, 0, 0},
        }))
    SigZ = matrix.Must(matrix.NewDense(
        [][]float64{
            {25, 0},
            {0, 25},
        }))
    FT = F.T(nil)
    HT = H.T(nil)

    MuT = matrix.Must(matrix.NewDense(
        [][]float64{
            {200},
            {0},
            {0},
            {0},
            {0},
            {0},
        }))

    predMuT = matrix.Must(matrix.NewDense(
        [][]float64{
            {200},
            {0},
            {0},
            {0},
            {0},
            {0},
        }))

    SigT = matrix.Must(matrix.NewDense(
        [][]float64{
            {0.1, 0, 0, 0, 0, 0},
            {0, 0.1, 0, 0, 0, 0},
            {0, 0, 100, 0, 0, 0},
            {0, 0, 0, 0.1, 0, 0},
            {0, 0, 0, 0, 0.1, 0},
            {0, 0, 0, 0, 0, 100},
        }))

    ZT, _ = matrix.NewDense(
        [][]float64{
            {200},
            {0},
        })

    var (
        host string
        port string
    )

    if len(os.Args) == 3 {
        host = os.Args[1]
        port = os.Args[2]
    } else {
        host = "localhost"
        port = "50101"
    }

    NUM_AGENTS = 1

    agentChannels = make([]chan bzrflag.Tank, NUM_AGENTS)
    for c := 0; c < NUM_AGENTS; c++ {
        agentChannels[c] = make(chan bzrflag.Tank, 10)
    }

    currentGoal = make([]goal, NUM_AGENTS)
    for g := 0; g < NUM_AGENTS; g++ {
        currentGoal[g].x = 0.0
        currentGoal[g].y = 0.0
    }

    renderGrid = make(chan string, 100)

    fmt.Printf("Starting Kalman Filter Agent\n\tHost: %s\n\tPort: %s\n", host, port)
    if conn.Connect(host, port) == false {
        fmt.Println("\tFailed to connect!")
        os.Exit(1)
    } else {
        fmt.Println("\tConnection Established.")
    }

    fmt.Println("\tSending Handshake.\n")
    conn.Write("agent 1\n")
    conn.ReadLine()
    conn.UnLock()

    WORLD_SIZE = bzrflag.GetConstantInt(conn, "worldsize")
    HALF_WORLD_SIZE = float64(WORLD_SIZE / 2)

    obstacleList = bzrflag.GetAllObstacles(conn)

    /**
     * Start Agent stuff
     */
    for t := 0; t < NUM_AGENTS; t++ {
        time.Sleep(50 * time.Millisecond)
        go RunKFAgent(t)
    }
    go ShowWindow()
    go UpdateFilter()
    UpdateTanks()

    /**
     * End Agent Stuff
     */

    fmt.Println("Terminating.")
    conn.Disconnect()
}

func UpdateTanks() {
    ticker := time.NewTicker(time.Millisecond * UPDATE_INTERVAL_MS)
    for {
        <-ticker.C
        response := conn.SendMessage("mytanks")
        if response == false {
            return
        }
        lines := conn.ReadList()
        for i := 0; i < len(lines); i++ {
            if strings.Contains(lines[i], "mytank ") {
                t := bzrflag.BuildTankFromData(lines[i])
                if t.Index < NUM_AGENTS {
                    (agentChannels[t.Index]) <- t
                }
            }
        }
        renderGrid <- "Go!"
    }
}

func UpdateFilter() {
    ticker := time.NewTicker(time.Millisecond * (1000 * DT_S))
    for {
        select {
        case <-ticker.C:
            response := conn.SendMessage("othertanks")
            if response == false {
                return
            }
            lines := conn.ReadList()
            eTank = bzrflag.BuildOtherTankFromData(lines[0])
            ZT.Set(0, 0, eTank.X)
            ZT.Set(1, 0, eTank.Y)
            // renderGrid <- "Go!"

            SBX := F.Dot(SigT, nil).Dot(FT, nil).Add(SigX, nil)

            SBX_Mult_HT := SBX.Dot(HT, nil)

            HMultSBX := H.Dot(SBX, nil)
            HSBXHT := HMultSBX.Dot(HT, nil)
            m := HSBXHT.Add(SigZ, nil)
            det := m.At(0, 0)*m.At(1, 1) - m.At(0, 1)*m.At(1, 0)
            m2 := matrix.Must(matrix.NewDense(
                [][]float64{
                    {m.At(1, 1), -1 * m.At(0, 1)},
                    {-1 * m.At(1, 0), m.At(0, 0)},
                }))
            Inverted := m2.Scalar(1/det, m2)
            Gain := SBX_Mult_HT.Dot(Inverted, nil)

            FMuT := F.Dot(MuT, nil)
            HFMuT := H.Dot(FMuT, nil)
            ZSubH := ZT.Sub(HFMuT, nil)
            GainMultZSubH := Gain.Dot(ZSubH, nil)
            MuT = FMuT.Add(GainMultZSubH, nil)

            Identity, _ := matrix.IdentityDense(6)
            GainMultH := Gain.Dot(H, nil)
            IdSubGMH := Identity.Sub(GainMultH, nil)
            SigT = IdSubGMH.Dot(SBX, nil)

            // fmt.Println(SigT)

            renderGrid <- "GO!"
        }
    }
}

func RunKFAgent(id int) {
    myTank := <-agentChannels[id]

    SigTTicker := time.NewTicker(time.Second * 5)

    for {
        select {
        case myTank = <-agentChannels[id]:
            dist := bzrflag.Distance(myTank.X, myTank.Y, MuT.At(0, 0), MuT.At(3, 0))
            leadTime := dist / 100.0

            FTemp := matrix.Must(matrix.NewDense(
                [][]float64{
                    {1, leadTime, math.Pow(leadTime, 2) / 2.0, 0, 0, 0},
                    {0, 1, leadTime, 0, 0, 0},
                    {0, -C, 1, 0, 0, 0},
                    {0, 0, 0, 1, leadTime, math.Pow(leadTime, 2) / 2.0},
                    {0, 0, 0, 0, 1, leadTime},
                    {0, 0, 0, 0, -C, 1},
                }))

            predMuT = FTemp.Dot(MuT, nil)
            // predMuT = F.Dot(MuT, nil)

            currentGoal[id].x = predMuT.At(0, 0)
            currentGoal[id].y = predMuT.At(3, 0)

            v := GetPFVector(myTank, currentGoal[id], obstacleList)
            angVel := GetAngVel(v.angle, myTank.Angle)
            myTank.Rotate(conn, angVel)
            if myTank.ShotsAvailable > 0 && math.Abs(lastError) < 0.09 {
                myTank.Shoot(conn)
            }

        case <-SigTTicker.C:
            fmt.Println(SigT)
        }
    }
}

func GetPFVector(t bzrflag.Tank, g goal, oList []bzrflag.Obstacle) vector {
    ALPHA := 0.01 // Attractive Scalar
    BETA := 0.06  // Repulsive Scalar
    GAMMA := 0.03 // Tangential Scalar

    REPUSLIVE_S := 20.0
    TANGENTIAL_S := REPUSLIVE_S + 20.0

    MIN_CLAMP := -1.0
    MAX_CLAMP := 1.0

    diffX := g.x - t.X
    diffY := g.y - t.Y
    distance := bzrflag.Distance(g.x, g.y, t.X, t.Y)
    theta := math.Atan2(diffY, diffX)

    dX := bzrflag.Clamp(ALPHA*distance*math.Cos(theta), MIN_CLAMP, MAX_CLAMP)
    dY := bzrflag.Clamp(ALPHA*distance*math.Sin(theta), MIN_CLAMP, MAX_CLAMP)

    for oIdx := 0; oIdx < len(oList); oIdx++ {
        o := oList[oIdx]

        diffX = o.Cx - t.X
        diffY = o.Cy - t.Y
        distance = bzrflag.Distance(o.Cx, o.Cy, t.X, t.Y)
        theta = math.Atan2(diffY, diffX)

        // Repulsive
        if distance < (o.R + REPUSLIVE_S) {
            dX -= bzrflag.Clamp(BETA*(REPUSLIVE_S+o.R-distance)*math.Cos(theta), MIN_CLAMP, MAX_CLAMP)
            dY -= bzrflag.Clamp(BETA*(REPUSLIVE_S+o.R-distance)*math.Sin(theta), MIN_CLAMP, MAX_CLAMP)
        }

        // Tangential
        if distance < (o.R + TANGENTIAL_S) {
            obsVX := o.Cx - g.x
            obsVY := o.Cy - g.y
            tankVX := t.X - g.x
            tankVY := t.Y - g.y
            dot := obsVX*tankVY - obsVY*tankVX
            var rotatedTheta float64 = math.Pi / 2.0
            if dot < 0 {
                rotatedTheta *= -1
            }

            dX -= bzrflag.Clamp(GAMMA*(TANGENTIAL_S+o.R-distance)*math.Cos(theta+rotatedTheta), MIN_CLAMP, MAX_CLAMP)
            dY -= bzrflag.Clamp(GAMMA*(TANGENTIAL_S+o.R-distance)*math.Sin(theta+rotatedTheta), MIN_CLAMP, MAX_CLAMP)
        }
    }

    var v vector
    v.angle = math.Atan2(dY, dX)
    v.magnitude = math.Sqrt(math.Pow(dX, 2) + math.Pow(dY, 2))
    if v.magnitude > 0 {
        v.magnitude = math.Max(v.magnitude, 0.5)
    }

    return v
}

const KP float64 = 1.0
const KD float64 = 1.0

var lastError float64 = 0.0

func GetAngVel(desiredAngle float64, currentAngle float64) float64 {
    angError := bzrflag.AngleDifference(desiredAngle, currentAngle)
    deltaError := (angError - lastError) / UPDATE_INTERVAL_MS
    angVel := KP*angError + KD*deltaError
    lastError = angError
    return angVel
}

/***************************************************
 * Functions for rendering the belief grid window
 ***************************************************/

func glfwErrorCallback(err glfw.ErrorCode, desc string) {
    fmt.Printf("%v: %v\n", err, desc)
}

func glPlotCircle(x float64, y float64, r float64) {
    var i int
    var lineAmount int = 100 //# of triangles used to draw circle

    twicePi := 2.0 * math.Pi

    gl.Begin(gl.LINE_LOOP)
    for i = 0; i <= lineAmount; i++ {
        gl.Vertex2d(
            x+(r*math.Cos(float64(i)*twicePi/float64(lineAmount))),
            y+(r*math.Sin(float64(i)*twicePi/float64(lineAmount))))
    }
    gl.End()
}

func ShowWindow() {
    glfw.SetErrorCallback(glfwErrorCallback)

    if !glfw.Init() {
        panic("Failed to initialize GLFW")
    }
    defer glfw.Terminate()

    window, err := glfw.CreateWindow(WORLD_SIZE, WORLD_SIZE, "Kalman Filter", nil, nil)
    if err != nil {
        panic(err)
    }

    window.MakeContextCurrent()

    gl.MatrixMode(gl.PROJECTION)
    gl.LoadIdentity()
    gl.Ortho(0, float64(WORLD_SIZE), 0, float64(WORLD_SIZE), 0, 1)
    gl.MatrixMode(gl.MODELVIEW)
    gl.Disable(gl.DEPTH_TEST)
    gl.ClearColor(0.0, 0.5, 0.5, 0.5)

    gl.Begin(gl.LINES)
    gl.Vertex2i(0, 400)
    gl.Vertex2i(800, 400)
    gl.Vertex2i(400, 0)
    gl.Vertex2i(400, 800)
    gl.End()

    for !window.ShouldClose() {
        // Wait for a signal to update the drawing
        <-renderGrid

        // gl.Clear(gl.COLOR_BUFFER_BIT)

        // gl.Begin(gl.POINTS)
        // for col := 0; col < WORLD_SIZE; col++ {
        //     for row := 0; row < WORLD_SIZE; row++ {
        //         gl.Color3f(1.0-float32(beliefGrid[col][row]),
        //             1.0-float32(beliefGrid[col][row]),
        //             1.0-float32(beliefGrid[col][row]))
        //         gl.Vertex2i(col, row)
        //     }
        // }

        // gl.End()

        // gl.Color3f(1.0, 0.0, 0.0)

        // gl.Begin(gl.QUADS)
        // for n := 0; n < NUM_AGENTS; n++ {
        //     gl.Vertex2d(currentGoal[n].x-5.0+HALF_WORLD_SIZE, currentGoal[n].y-5.0+HALF_WORLD_SIZE)
        //     gl.Vertex2d(currentGoal[n].x+5.0+HALF_WORLD_SIZE, currentGoal[n].y-5.0+HALF_WORLD_SIZE)
        //     gl.Vertex2d(currentGoal[n].x+5.0+HALF_WORLD_SIZE, currentGoal[n].y+5.0+HALF_WORLD_SIZE)
        //     gl.Vertex2d(currentGoal[n].x-5.0+HALF_WORLD_SIZE, currentGoal[n].y+5.0+HALF_WORLD_SIZE)
        // }
        // gl.End()

        // gl.Begin(gl.LINES)
        // for r := 0; r < OBSTACLE_GRANULARITY; r++ {
        // 	gl.Vertex2i(0, r*int(OBSTACLE_SIZE))
        // 	gl.Vertex2i(WORLD_SIZE, r*int(OBSTACLE_SIZE))
        // }
        // for c := 0; c < OBSTACLE_GRANULARITY; c++ {
        // 	gl.Vertex2i(c*int(OBSTACLE_SIZE), 0)
        // 	gl.Vertex2i(c*int(OBSTACLE_SIZE), WORLD_SIZE)
        // }
        // gl.End()

        gl.Color3f(1.0, 1.0, 1.0)

        gl.Begin(gl.LINES)
        gl.Vertex2i(0, 400)
        gl.Vertex2i(800, 400)
        gl.Vertex2i(400, 0)
        gl.Vertex2i(400, 800)
        gl.End()

        gl.Color3f(0.0, 1.0, 0.0)

        glPlotCircle(eTank.X+HALF_WORLD_SIZE, eTank.Y+HALF_WORLD_SIZE, 2.0)

        gl.Color3f(0.0, 0.0, 1.0)

        glPlotCircle(MuT.At(0, 0)+HALF_WORLD_SIZE, MuT.At(3, 0)+HALF_WORLD_SIZE, 5.0)

        gl.Color3f(1.0, 0.0, 0.0)

        glPlotCircle(predMuT.At(0, 0)+HALF_WORLD_SIZE, predMuT.At(3, 0)+HALF_WORLD_SIZE, 5.0)

        // gl.Color3f(1.0, 1.0, 1.0)
        // glPlotCircle(currentGoal[0].x+HALF_WORLD_SIZE, currentGoal[0].y+HALF_WORLD_SIZE, 10.0)

        window.SwapBuffers()
        glfw.PollEvents()
    }
}
