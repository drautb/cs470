package bzrflag

import (
	"fmt"
	"strings"
	"tcpconn"
)

type Flag struct {
	Owner     string
	Possessor string
	X         float64
	Y         float64
}

const FLAG_FIELD_COUNT int = 4

func BuildFlagFromData(dataStr string) Flag {
	fields := strings.Fields(dataStr)
	if len(fields) != FLAG_FIELD_COUNT+1 {
		fmt.Println("Error parsing flag data string1")
		fmt.Printf("\tLine: %s", dataStr)
		fmt.Printf("\tFields Length: %d\n", len(fields))
	}

	var f Flag
	f.Owner = fields[1]
	f.Possessor = fields[2]
	f.X = StrToFloat(fields[3])
	f.Y = StrToFloat(fields[4])

	return f
}

func GetAllFlags(tcpConn tcpconn.TCPConn) []Flag {
	fList := make([]Flag, 1)
	response := tcpConn.SendMessage("flags")
	if response == false {
		return fList
	}
	lines := tcpConn.ReadList()
	for i := 0; i < len(lines); i++ {
		f := BuildFlagFromData(lines[i])
		fList = append(fList, f)
	}
	return fList
}

func GetFlag(tcpConn tcpconn.TCPConn, flagColor string) Flag {
	flags := GetAllFlags(tcpConn)
	for f := 0; f < len(flags); f++ {
		if flags[f].Owner == flagColor {
			return flags[f]
		}
	}
	return flags[0]
}
