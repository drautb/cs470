package bzrflag

import (
	"strconv"
	"strings"
	"tcpconn"
)

func GetConstantInt(conn tcpconn.TCPConn, name string) int {
	constants := getConstantMap(conn)
	val, err := strconv.Atoi(constants[name])
	if err != nil {
		panic(err)
	}

	return val
}

func GetConstantFloat(conn tcpconn.TCPConn, name string) float64 {
	constants := getConstantMap(conn)
	val, err := strconv.ParseFloat(constants[name], 64)
	if err != nil {
		panic(err)
	}

	return val
}

func getConstantMap(conn tcpconn.TCPConn) map[string]string {
	response := conn.SendMessage("constants")
	if response == false {
		panic("Constants request failed!")
	}
	lines := conn.ReadList()
	constants := make(map[string]string)
	for c := 0; c < len(lines); c++ {
		fields := strings.Fields(lines[c])
		constants[fields[1]] = fields[2]
	}

	return constants
}
