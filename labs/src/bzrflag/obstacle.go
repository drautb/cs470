package bzrflag

import (
	"fmt"
	"math"
	"strings"
	"tcpconn"
)

type Obstacle struct {
	Px []float64 // All x coordinates of this obstacles verticies
	Py []float64 // All y coordinate of this obstacle's vertices
	Cx float64
	Cy float64
	R  float64
}

func BuildObstacleFromData(dataStr string) Obstacle {
	fields := strings.Fields(dataStr)
	if fields[0] != "obstacle" {
		fmt.Println("Error parsing obstacle data string!")
		fmt.Printf("\tLine: %s", dataStr)
		fmt.Printf("\tFields Length: %d\n", len(fields))
	}

	var o Obstacle
	o.Px = make([]float64, 0)
	o.Py = make([]float64, 0)
	for p := 1; p < (len(fields) - 1); p += 2 {
		o.Px = append(o.Px, StrToFloat(fields[p]))
		o.Py = append(o.Py, StrToFloat(fields[p+1]))
	}

	o.Cx = Mean(o.Px)
	o.Cy = Mean(o.Py)

	o.R = o.CalculateRadius()

	return o
}

func GetAllObstacles(tcpConn tcpconn.TCPConn) []Obstacle {
	oList := make([]Obstacle, 0)
	response := tcpConn.SendMessage("obstacles")
	if response == false {
		return oList
	}
	lines := tcpConn.ReadList()
	for i := 0; i < len(lines); i++ {
		o := BuildObstacleFromData(lines[i])
		oList = append(oList, o)
	}
	return oList
}

func (o *Obstacle) CalculateRadius() float64 {
	greatestDistance := 0.0
	for i := 0; i < len(o.Px); i++ {
		dist := Distance(o.Px[i], o.Py[i], o.Cx, o.Cy)
		if dist > greatestDistance {
			greatestDistance = dist
		}
	}
	return greatestDistance
}

func (o *Obstacle) Distance(o2 *Obstacle) float64 {
	return math.Sqrt(o.DistanceSq(o2))
}

func (o *Obstacle) DistanceSq(o2 *Obstacle) float64 {
	return (math.Pow((o2.Cx-o.Cx), 2) + math.Pow((o2.Cy-o.Cy), 2))
}

func (o *Obstacle) Overlaps(o2 *Obstacle) bool {
	return o.DistanceSq(o2) < math.Pow((o.R+o2.R), 2)
}

func (o *Obstacle) Contains(o2 *Obstacle) bool {
	return o.R > o.Distance(o2)+o2.R
}

func (o *Obstacle) Midpoint(o2 *Obstacle) (float64, float64) {
	// fmt.Println("Calculating midpoint for two obstacles...")
	// fmt.Printf("o1.Cx: %f, o1.Cy: %f\no2.Cx: %f, o2.Cy: %f\n\n", o.Cx, o.Cy, o2.Cx, o2.Cy)
	return (o2.Cx + o.Cx) / 2.0, (o2.Cy + o.Cy) / 2.0
}

func (o *Obstacle) Consolidate(o2 *Obstacle, MAX_RADIUS float64) Obstacle {
	if o.Contains(o2) {
		return *o
	} else if o2.Contains(o) {
		return *o2
	}

	var newObs Obstacle
	newObs.Cx, newObs.Cy = o.Midpoint(o2)
	newObs.R = math.Min(o.Distance(o2)+((o.R+o2.R)/2.0), MAX_RADIUS)

	return newObs
}
