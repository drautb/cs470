package bzrflag

import (
	"fmt"
	"strings"
	"tcpconn"
)

type Base struct {
	Team string
	Tx   float64
	Ty   float64
	Bx   float64
	By   float64
	Cx   float64
	Cy   float64
}

const BASE_FIELD_COUNT int = 9

func BuildBaseFromData(dataStr string) Base {
	fields := strings.Fields(dataStr)
	if len(fields) != BASE_FIELD_COUNT+1 {
		fmt.Println("Error parsing base data string1")
		fmt.Printf("\tLine: %s", dataStr)
		fmt.Printf("\tFields Length: %d\n", len(fields))
	}

	var b Base
	b.Team = fields[1]
	b.Tx = StrToFloat(fields[8])
	b.Ty = StrToFloat(fields[9])
	b.Bx = StrToFloat(fields[4])
	b.By = StrToFloat(fields[5])

	b.Cx = b.Tx + ((b.Bx - b.Tx) / 2.0)
	b.Cy = b.Ty + ((b.By - b.Ty) / 2.0)

	return b
}

func GetAllBases(tcpConn tcpconn.TCPConn) []Base {
	bList := make([]Base, 0)
	response := tcpConn.SendMessage("bases")
	if response == false {
		return bList
	}
	lines := tcpConn.ReadList()
	for i := 0; i < len(lines); i++ {
		b := BuildBaseFromData(lines[i])
		bList = append(bList, b)
	}
	return bList
}

func GetBase(tcpConn tcpconn.TCPConn, baseColor string) Base {
	bases := GetAllBases(tcpConn)
	for b := 0; b < len(bases); b++ {
		if bases[b].Team == baseColor {
			return bases[b]
		}
	}
	return bases[0]
}

func GetBaseFromList(bList []Base, baseColor string) Base {
	for b := 0; b < len(bList); b++ {
		if bList[b].Team == baseColor {
			return bList[b]
		}
	}
	return bList[0]
}
