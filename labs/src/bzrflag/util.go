package bzrflag

import (
	"math"
	"math/rand"
	"time"
)

func RandomDuration(low int32, high int32) time.Duration {
	interval := high - low
	randN := rand.Int31n(interval)
	ms := low + randN
	return time.Duration(ms) * time.Millisecond
}

func AngleDifference(desired float64, current float64) float64 {
	angDiff := desired - current
	if angDiff > math.Pi {
		angDiff -= 2 * math.Pi
	} else if angDiff < -math.Pi {
		angDiff += 2 * math.Pi
	}
	return angDiff
}

func Distance(x1 float64, y1 float64, x2 float64, y2 float64) float64 {
	return math.Sqrt(math.Pow(x2-x1, 2) + math.Pow(y2-y1, 2))
}

func Clamp(x float64, min float64, max float64) float64 {
	if x < min {
		return min
	} else if x > max {
		return max
	}

	return x
}

func Mean(vals []float64) float64 {
	sum := 0.0
	for i := 0; i < len(vals); i++ {
		sum += vals[i]
	}
	return sum / float64(len(vals))
}
