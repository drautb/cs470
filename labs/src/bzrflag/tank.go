package bzrflag

import (
	"fmt"
	"strconv"
	"strings"
	"tcpconn"
)

type Tank struct {
	Index          int    // 0-based index on the team
	Callsign       string // red0, green2, etc
	Team           string // red, blue, etc
	Status         string // "alive"
	ShotsAvailable int
	TimeToReload   float64 // seconds til reloaded
	HasFlag        string  // String of the flag this guy has, or - if nothing
	X              float64
	Y              float64
	Angle          float64
	Vx             float64
	Vy             float64
	Angvel         float64
}

const TANK_FIELD_COUNT int = 12
const ENEMY_TANK_FIELD_COUNT int = 7

func BuildTankFromData(dataStr string) Tank {
	fields := strings.Fields(dataStr)
	if len(fields) != TANK_FIELD_COUNT+1 {
		fmt.Println("Error parsing tank data string!")
		fmt.Printf("\tLine: %s", dataStr)
		fmt.Printf("\tFields Length: %d\n", len(fields))
	}

	var t Tank
	t.Index = StrToInt(fields[1])
	t.Callsign = fields[2]
	t.Team = t.Callsign[0 : len(t.Callsign)-1]
	t.Status = fields[3]
	t.ShotsAvailable = StrToInt(fields[4])
	t.TimeToReload = StrToFloat(fields[5])
	t.HasFlag = fields[6]
	t.X = StrToFloat(fields[7])
	t.Y = StrToFloat(fields[8])
	t.Angle = StrToFloat(fields[9])
	t.Vx = StrToFloat(fields[10])
	t.Vy = StrToFloat(fields[11])
	t.Angvel = StrToFloat(fields[12])

	return t
}

func BuildOtherTankFromData(dataStr string) Tank {
	fields := strings.Fields(dataStr)
	if len(fields) != ENEMY_TANK_FIELD_COUNT+1 {
		fmt.Println("Error parsing tank data string!")
		fmt.Printf("\tLine: %s", dataStr)
		fmt.Printf("\tFields Length: %d\n", len(fields))
	}

	var t Tank
	t.Callsign = fields[1]
	t.Team = t.Callsign[0 : len(t.Callsign)-1]
	t.Status = fields[3]
	t.HasFlag = fields[4]
	t.X = StrToFloat(fields[5])
	t.Y = StrToFloat(fields[6])
	t.Angle = StrToFloat(fields[7])

	return t
}

func StrToInt(s string) int {
	i, err := strconv.ParseInt(s, 0, 0)
	if err != nil {
		fmt.Println("Error in StrToInt: ", err)
	}
	return int(i)
}

func StrToFloat(s string) float64 {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		fmt.Println("Error in StrToFloat: ", err)
	}
	return f
}

func (tank *Tank) Move(conn tcpconn.TCPConn, velocity float64) bool {
	command := fmt.Sprint("speed ", tank.Index, " ", velocity)
	response := conn.SendMessage(command)
	if response {
		conn.ReadStatus()
		return strings.Contains(conn.Line, "ok")
	}
	return false
}

func (tank *Tank) Rotate(conn tcpconn.TCPConn, angVel float64) bool {
	command := fmt.Sprint("angvel ", tank.Index, " ", angVel)
	response := conn.SendMessage(command)
	if response {
		conn.ReadStatus()
		return strings.Contains(conn.Line, "ok")
	}
	return false
}

func (tank *Tank) Shoot(conn tcpconn.TCPConn) bool {
	command := fmt.Sprint("shoot ", tank.Index)
	response := conn.SendMessage(command)
	if response {
		conn.ReadStatus()
		return strings.Contains(conn.Line, "ok")
	}
	return false
}
