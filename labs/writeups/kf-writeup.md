<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<style media="screen" type="text/css">
body {
    font-size: 10pt;
}
</style>

Kalman Filter Writeup
=====================

**Ben Draut / CS 470 / December 12, 2013**

I worked alone and spent 16 hours on this lab.

# Matrix Discussion

I found that lower values in $\Sigma_x$ yielded better results. I ended up
using 0.01 for the position noise, 0.1 for the velocity noise, and 5 for the
acceleration noise. I also hadn't realized before the connection between
$\Sigma_t$ and $\Sigma_x$ and $\Sigma_z$, so at first I was using values that
were making $\Sigma_t$ much too large.

Once $\mu_t$ was updated, I used it to calculate the distance between my tank
and the enemy. I then generated a new $F$ matrix with a time interval
proportional to this distance instead of the fixed interval. Applying this new
$F$ matrix to $\mu_t$ then gave me the predicted position of the enemy tank.

# Abilities and Limitations

The Kalman filter works really well with linear models, such as the physics
models we've been dealing with. However, it cannot effectively model any sort
of circular motion. My non-conforming pigeon just drove in large circles, and
it was nearly impossible for the Kalman filter to hit it. It would always
overshoot the actual future position of the tank.
 observations in this lab.

# Visualizations

The following are examples of how I visualized my filter. Green circles
represent the raw noisy position obtained from the server. Blue circles
represent the position after passing through the Kalman filter. Red circles
represent my predicted position following an observation.

![](../../../Images/kf-1.png)

<br/>

![](../../../Images/kf-2.png)
