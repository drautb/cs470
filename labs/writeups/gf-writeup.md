Grid Filter Lab
===============

**Ben Draut / CS 470 / Nov 21, 2013**

I worked alone and spent 19 hours on this lab.


## Summary

This lab was really interesting. I gained a little more appreciation for how
the grid filter readings improve with multiple scans, and how the probabilities
affect the readings. I also learned a lot about how to get my agents to explore
the world in an intelligent way, and how to deal with getting stuck and other
odd cases. I think the most interesting part for me was learning how to take
the filtered sensor readings, and then convert that into some sort of obstacle
representation that the tanks could deal with.


## Implementation Discussion

I'm writing my projects for this class in Go, which I had never previously used.
I'm still learning to use its concurrency mechanisms correctly, but overall its
going well. Due to this however, I would get some interesting bugs in my tanks'
movements if I used more than just a small handful. I opted to use four tanks,
and to divide the world up into four quadrants, one for each tank to search.

Initially, each tank started at the lower-left corner of its quadrant and then
zigzagged back and forth exploring the world and taking sensor readings. They
did this by just moving a goal point back and forth, and then using the PD
controller from lab 1. A separate process examined the belief grid looking for
clusters of pixels where the belief that an obstacle is there exceeded a
certain threshold. When such a cluster was found, it created an obstacle object
there in the program for the tanks to use as they explored.

This initially led to many small obstacle representations. To improve
performance and hopefully help some tanks avoid getting stuck in tight spaces,
I decided to have another process that would examine existing obstacles, and
consolidate them if they were the same size and overlapped. I found that this
worked reasonably well for guiding the tanks around some trickier obstacles. I
also needed to put a limit on how large these obstacles could be, otherwise
they would sometimes end up enveloping the entire world.

I also took advantage of this process to improve the belief grid. If an
obstacle was placed at a certain location, I checked to see if the belief
exceeded an even higher threshold. If it did, I filled the area with 1's,
removing all doubt. Likewise, if the belief for the region was extremely low, I
set all the pixels to 0. This effectively tidied up the belief grid even though
just a few sensor readings had been taken.

When tanks became stuck, I decided to just give them a new goal that
corresponded to a random unexplored point in the world. This was fairly
effective in removing them from tight spots. In addition, the tanks helped
each other explore their quadrants, so they were less likely to get stuck. (One
tank could reach a goal that the other would get stuck in trying to reach.)


## Sensor Variation Discussion

I was really impressed with how quickly a grid filter could build up an
accurate representation of the world from just a few readings when using the
probabilities given. 

If the sensor had a bigger range, it would allow the tanks to explore the world
more quickly, but it would also require more computation. If it's estimates
were noisier, I would have to make my tanks explore much slower. In my
implementation, each tank took a reading about twice a second. If I tried to do
it more frequently than this, it caused some issues. A noisier sensor would
mean that more readings would be required of the same area to increase
confidence, so I would have to slow my tanks way down.

If the model were incorrect, that would be difficult. Hopefully I overestimated
the amount of noise it returned. If so, I would be protected by compensating to
additional non-existent noise. If I underestimated however, that could be very
bad. I think I would end up thinking that more obstacles were there than
actually existed.

If sensor only detected moving objects, that would change everything. It
wouldn't make sense to use such a sensor to build a model of where static
obstacles were in the world. Using such a sensor to detect enemy tanks perhaps
would be interesting. I think I would have to have some measure of how old each
reading was, so as to determine the most likely current location of each tank,
and readings would have to just disappear after they were more than a second
or two old. (It sounds kind of like a Kalman filter...)


## Tournament Application

I think I would use the grid filter in the tournament as a scouting tool to
build up the environment and be able to plan better. I would have a few agents
that would go out and scout around, (using the filter) while the rest would
stay back and explore closer to home base, or just defend the flag. Then, once
my agents had a solid model of the world, the other agents could plan more
offensive strategies.


