<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<style type="text/css">
    .img-container {
        width: 100%;
        text-align: center;
    }

    img {
        width: 350px;
    }
</style>

Potential Fields Lab
====================

**Ben Draut / CS 470 / Oct. 11, 2013**

I worked alone and spent 15 hours on this lab.


## Dumb Agent

Complete.


## Potential Fields Implementation

I implemented attractive, repulsive, and tagnetial potential fields for this lab, per the specifications. I decided to use MATLAB to model the fields, and then mirror the calculations in my agent's code. I found that this typically worked extremely well, requiring minimal tweaking afterwards. 

I also decided to clamp all values to the range [-1.0, 1.0], corresponding to the limits of the tank's velocity. This way, I could just set the tank's speed to be the magnitude of the vector obtained from the potential field calculations. 


### Attractive Fields

Attractive fields are used to attract the agent towards its goal, and are calculated using the following formula:

$\Delta x = \alpha (d Cos(\theta))$, 
$\Delta y = \alpha (d Sin(\theta))$

Where $\alpha$ is a constant (0.01), $d$ is the distance from the agent to the goal, and $\theta$ is the angle between the agent and the goal.

<div class="img-container">
    <img src="../../plots/attractive-rb.png"/>
    <img src="../../plots/attractive-fl.png"/>
</div>


### Repulsive Fields

Repulsive fields are used to deter the agents from running into obstacles, and are calculated using the following formula:

$\Delta x = \beta (r+s-d) Cos(\theta)$,
$\Delta y = \beta (r+s-d) Sin(\theta)$

Where $\beta$ is a constant (0.06), $r$ is the radius of the obstacle, $s$ is how far outside the radius this obstacle has an influence, $d$ is the distance from the agent to the center of the obstacle, and $\theta$ is the angle between the agent and the obstacle.

<div class="img-container">
    <img src="../../plots/repulsive-rb.png"/>
    <img src="../../plots/repulsive-fl.png"/>
</div>


### Tangential Fields

Tangential fields are used to help the agents navigate around obstacles, and are calculated using the following formula:

$\Delta x = \gamma (r+s-d) Cos(\theta + \phi)$,
$\Delta y = \gamma (r+s-d) Sin(\theta + \phi)$

Where $\gamma$ is a constant (0.03), $r$ is the radius of the obstacle, $s$ is how fare outside the radius this obstacle exerts and influence, $d$ is the distance from the agent to the center of the obstacle, $\theta$ is the angle between the agent and the obstacle, and $\phi$ is an rotational offset. (Either $-\pi/2$ or $\pi/2$)

<div class="img-container">
    <img src="../../plots/tangential-rb.png"/>
    <img src="../../plots/tangential-fl.png"/>
</div>


### Combined Fields

I combined all three fields by simply adding together the $\Delta x, \Delta y$ values obtained from each.

<div class="img-container">
    <img src="../../plots/combined-rb.png"/>
    <img src="../../plots/combined-fl.png"/>
</div>

#### Tuning

Most of the time tuning was spent in MATLAB. It took a fair amount of work to find a good balance between the $\alpha$, $\beta$, and $\gamma$ constants. At first, the attractive field was far too powerful, so that although the repulsive and tangential fields existed, they exhibited no visual effect on the motion of the agent.

Once I did find a decent combination, the attractive field was balanced with the other two, but it became extremeley weak as the agent approached the goal. It would take nearly a full minute for the agent to travel the last 40 units or so to the flag. To compensate for this, I decided clamp the agent's minimum velocity at 0.5. This allows it to slow down enough to maneuver, without taking an unreasonable amount of time to reach the goal.

I also spent a little bit of time experimenting with different values of $s$ for the repulsive and tangential fields. I could get the agent's movement to appear a little more smooth and natural by causing the $s_{tangential}$ to extend beyond $s_{repulsive}$. This way, the tangential force will apply before the agent gets too close to the obstacle.

The other issue I encountered with the tangential fields was deciding which way the field should spin. (Whether $\phi$ should be + or -) I wanted to spin the agent in the direction that would enable it to take the shortest path around an obstacle to the goal. I ended up taking the angle between the vectors from the goal to the center of the obstacle, and from the goal to the agent. Based on the sign of this angle, I knew which way to spin the field.

This seems to work pretty well for the most part. The only downside is that it can still get stuck when obstacles are grouped together. (Like in the four_ls world) The way that I would resolve this would be to treat closely grouped obstacles as one obstacle. Doing so would keep the agent away from the whole group, thus preventing it from getting stuck. Unfortunately, I didn't have time to implement this. 

I didn't have to do much tuning with my PD controller. Both constants are just 1.0, and it works well. The one issue that did take some work was making sure that it turned the correct direction. I used the modulo operator to help get the shortest angle (preserving the sign) between two vectors, but it wasn't working for some reason. Come to find out that Go doesn't implement the modulo operator the same way that most languages do for negative numbers. Once I used a different formula that didn't need the %, it worked great.


## Testing

You must also comment on your results for each of the test scenarios listed above. Do not just say "we won". What cool behaviors have you been able to create in your implementation? How did you do against someone else's dumb agent?
You must report (briefly) on the results the "other group" had against you. Include their names to so that we can verify that everyone "passed off" against someone. Also give the names of the people you passed off against, again, make sure you say how you did.

### My PF vs. My DA (1)

In both worlds, my PF agent won, though the dumb agent did get a lucky shot off sometimes. One additional tweak I had to make 


### My PF vs. My DA (2)

In both worlds, my PF agent won. Again, sometimes the dumb agents would get some lucky shots and make the PF agent start over.


### My PF vs. My PF

The agents' behavior nearly mirrored each other. They would both take the same circular path back and forth between their bases capturing the flag and going back. Unless they got lucky and managed to shoot each other, they primary factor in who won seemed just to be whichever agent I started first. This was the same in both worlds.


### My PF vs. Their DA (1)

> I gave my agents to the following groups for them to test against:
>
> * Ryan Hansen and Stephen Kitto
> * Jordan Johnson
> * Sam Miller
> * Ben Hansen
>
> For my own tests, I tested against Sam Miller's agents.

In both worlds, my PF agent won. His agent would wander more than my dumb ones did, so it was interesting to see the different points where our agents crossed paths.


### My PF vs. Their DA (2)

In both worlds, my PF agent won. Same results as above.


### My PF vs. Their PF

This was a ton of fun. I didn't add any "cool" behavior to my agent, it simply goes to the flag, then heads back to base. Sam's agent did some interesting stuff (on accident) that made it really interesting though. Sam had his agent set up to return to his own flag's location, rather than his base, once it had captured a flag. So, if I had Sam's flag, as soon as he grabbed mine, his agent started pursuing me! My agent was a bit more nimble when it came to manuevering around obstacles though, so it was fun to watch mine try to escape from him. (I still ended up winning)


