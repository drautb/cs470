<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Bayes Rule Homework
===================

Ben Draut / CS 470 / October 9, 2013

**1) (13.3) For each of the following statements either prove it is true or give a counterexample.**

**a.** If $P(a|b,c) = P(b|a,c)$, then $P(a|c) = P(b|c)$

**True. Proof:**

$P(a|b,c) = P(b|a,c)$

Using the definition of conditional distributions we can rewrite this as:

$\frac{P(a,b,c)}{P(b,c)} = \frac{P(a,b,c)}{P(a,c)}$

Multiplying both sides by $P(a,c)$ and dividing both sides by $P(a,b,c)$ gives:

$\frac{P(a,c)}{P(b,c)} = 1$

Therefore, $P(a|c) = P(b|c)$.

<br/>


**b.** If $P(a|b,c) = P(a)$, then $P(b|c) = P(b)$

**False.** $P(a|b,c) = P(a)$ tells us that _a_ is independent of _b_ and _c_. It tells us _nothing_ about the relationship between _b_ and _c_, therefore we cannot deduce that $P(b|c) = P(b)$.

**Counterexample:** 

* Let _a_ be a value from the set $\\{1,2,3,4,5,6\\}$. 
* Let _b_ be a value from the set $\\{AttendSchool, \neg AttendSchool\\}$. 
* Let _c_ be a value from the set $\\{Sick, \neg Sick\\}$.

_a_ is the outcome of me rolling a fair die, _b_ is whether or not I go to school on a given day, and _c_ is whether or not I'm sick on that day. $P(a|b,c) = P(a)$ because whether or not I go to school and am sick has no influence on the outcome of the die roll. However, $P(b|c) \neq P(b)$ because whether or not I'm sick _definitely_ will influence whether or not I go to school.

<br/>


**2) (13.8) Given the full joint distribution shown in Figure 13.3, calculate the following:**

**a.** $\mathbf{P}(toothache)$

Let $toothache = t$, $catch = ct$, $cavity = cv$.

$\mathbf{P}(t) = P(t, ct, cv) + P(t, ct, \neg cv) + P(t, \neg ct, cv) + P(t, \neg ct, \neg cv)$

$\mathbf{P}(t) = 0.108 + 0.016 + 0.012 + 0.064$

$\mathbf{P}(t) = 0.20$

<br/>


**b.** $\mathbf{P}(Cavity)$

$\mathbf{P}(Cavity) = [P(cavity), P(\neg cavity)]$

$P(cavity) = 0.2$ (Pg. 492). $P(\neg cavity) = 1 - P(cavity)$, Therefore:

$\mathbf{P}(Cavity) = [0.2, 0.8]$

<br/>


**3) This problem comes from G. Gigerenzer, "Calculated Risks: How To Know When Numbers Deceive You", Simon and Schuster Press, 2002. Give the answer and show how you obtain the results using Bayes rule.**

To diagnose colorectal cancer, the hemoccult test --- among others --- is conducted to detect occult blood in the stool. This test is used from a particular age on, but also in routine screening for early detection of colorectal cancer. Imagine you conduct a screening using a hemoccult test in a certain region. For symptom-free people over 50 years old who participate in screening using the hemoccult test, the following information is available for this region.

The probability that one of these people has colorectal cancer is 0.3 percent. If a person has colorectal cancer, the probability is 50 percent that this person will have a positive hemoccult test. If a person does not have colorectal cancer, the probability is 3 percent that this person will still have a positive hemoccult test. Imagine a person (over age 50, no symptoms) who has a positive hemoccult test in your screening. What is the probability that this person actually has colorectal cancer.

$S:\Omega \to \\{cancer, \neg cancer\\}$

$O:\Omega \to \\{positive, negative\\}$

$P_S(cancer) = 0.003, P_S(\neg cancer) = 0.997$

<table>
    <tr><th>$P_{O|S}(o|s)$</th><th>$o$</th><th>$s$</th></tr>
    <tr><td>$0.50$</td><td>$Positive$</td><td>$Cancer$</td></tr>
    <tr><td>$0.50$</td><td>$Negative$</td><td>$Cancer$</td></tr>
    <tr><td>$0.03$</td><td>$Positive$</td><td>$\neg Cancer$</td></tr>
    <tr><td>$0.97$</td><td>$Negative$</td><td>$\neg Cancer$</td></tr>
</table>

Calculate $P(Cancer|Positive)$.

According to Baye's Rule:

$P(Cancer|Positive) = \frac{P_{O|C}(Positive|Cancer)P_S(Cancer)}{P_O(Positive)}$

$P_O(Positive) = (P_{O|S}(Pos.|Can.)P_S(Can.)) + (P_{O|S}(Pos.|\neg Can.)P_S(\neg Can.))$

$P_O(Positive) = ((0.50)(0.003)) + ((0.03)(0.997))$

$P_O(Positive) = 0.0314$

$\frac{P_{O|S}(Positive|Cancer)P_S(Cancer)}{P_O(Positive)} = \frac{(0.90)(0.003)}{0.0314}$

$\frac{P_{O|S}(Positive|Cancer)P_S(Cancer)}{P_O(Positive)} = 0.0860$

Therefore, $P(Cancer|Positive) = 8.6\%$
