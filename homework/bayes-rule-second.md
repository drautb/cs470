<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Bayes Rule Homework The Second
==============================

Ben Draut / CS 470 / Oct. 11, 2013

**1. (Adapted from Pearl (1988) ). Three prisoners, A, B, and C, are locked in their cells. It is common knowledge that one of them is to be executed the next day and the others are to be pardoned. Only the Governor knows which one will be executed. Prisoner A asks the guard a favor: "Please ask the Governor who will be executed, and then take a message to one of my friends B or C to let him know that he will be pardoned in the morning." The Guard agrees, and comes back later and tells A that he gave the pardon message to B. What are A's chances of being executed, given this information? Show how you obtain this answer using Bayes rule.**

$S$ corresponds to the prisoner that is executed. $O$ corresponds to the prisoner that receives the pardon message.

$S:\Omega \to \\{A, B, C\\}$

$O:\Omega \to \\{A, B, C\\}$

$P_S(A) \equiv P_S(B) \equiv P_S(C) \equiv \frac{1}{3}$

<table>
    <tr><th>$P_{O|S}(o|s)$</th><th>$o$ (Pardoned)</th><th>$s$ (Executed)</th></tr>
    <tr><td>$0.50$</td><td>$B$</td><td>$A$</td></tr>
    <tr><td>$0.00$</td><td>$B$</td><td>$B$</td></tr>  
    <tr><td>$1.00$</td><td>$B$</td><td>$C$</td></tr>
</table>

Compute $P_{S|O}(A|B)$

According to Baye's Rule:

$P_{S|O}(A|B) = \frac{P_{O|S}(B|A)P_S(A)}{P_O(B)}$

$P_O(B) = \sum_{s}^{} P_{O|S(B|s)P_S(s)}$

$P_O(B) = P_{O|S}(B|A)P_S(A) + P_{O|S}(B|B)P_S(B) + P_{O|S}(B|C)P_S(C)$

$P_O(B) = (0.50)(0.33) + (0.00)(0.33) + (1.00)(0.33)$

$P_O(B) = 0.50$

$\frac{P_{O|S}(B|A)P_S(A)}{P_O(B)} = \frac{(0.50)(0.33)}{0.50}$

$\frac{P_{O|S}(B|A)P_S(A)}{P_O(B)} = 0.3333$

Therefore, $P_{S|O}(A|B) = 33\%$. It is unchanged. However, we can calculate $P_{S|O}(A|C) = 66\%$, and see that C's probability of being executed has doubled.


**2. Suppose that you were going to write a small program that simulates what message the Guard gives to prisoner A. What message would the Guard bring if prisoner C was being executed? If prisoner B was being executed? If prisoner A was being executed?**

* C Being Executed: B was pardoned.

* B Being Executed: C was pardoned.

* A Being Executed: Choose randomly between B and C, deliver pardoned message to the chosen prisoner.


**3. Write a small program that randomly selects which of the three prisoners will be executed. Use the model of the Guard that you created in part 2 and report which message is delivered to prisoner A. Run the program several times, keeping track of which prisoner would actually be executed whenever the Guard delivers a message to prisoner A stating that prisoner B gets pardoned. What percentage of time was prisoner A the one who would have been executed? Prisoner B? Prisoner C?**


    Performing Prisoner Analysis with 1000000 iterations.
    Anaylsis Complete!

    Results:
    B was given the pardon message 499634 times.

    A was executed 166353 times.
    B was executed 0 times.
    C was executed 333281 times.

    A Execution Percentage: 33%
    B Execution Percentage: 0%
    C Execution Percentage: 66%
