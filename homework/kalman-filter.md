Kalman Filter Homework
======================

Ben Draut / CS 470 / Nov. 22, 2013

**1. Complete the Kalman MATLAB tutoial, and summarize what you've learned in one
or two paragraphs.**

This tutorial helped a lot. I feel like I finally got a better understanding of
the purpose of the Kalman filter, and when it can be used. I understand better
now how the Kalman filter can absorb a lot of the noise in observations and
states, and still predict fairly accurate values in a continuous space.

The most helpful exercise was adjusting the system and observation noise in the
last example. When I increased the system noise, it didn't seem to change too
much, and the filter could still get a pretty good idea of where the target
was. I was amazed though when I made the observation noise really high, and
even though the observed states were nowhere close to the actual state, the
filter still did a really good job of predicting the true state!


**2. Repeat the steps done in C7 (concept 7) of the tutorial, but set sig0=10.5
and sest = s + 1.5 * randn(2,1). Explain the differences from the tutorial
results.**

When sig0=10.5 and sest = s + 1.5 * randn(2,1), the initial variance in our
estimate is enormous. The simulation starts a guess that the noise in the
observation is huge, so the first contour map envelops nearly the entire space.


**3. Repeat the steps done in the previous problem, but set sig0=1.5 and
sest=s+10.5*randn(2,1). Explain the differences from the tutorial results.**

When sig0=1.5 and sest=s+10.5*rand(2,1), the initial guess is farther away from
the the true state. It takes several cycles to get closer to the state, while
when the initial guess is better (like it is originally) the filter gets closer
to the true state more quickly.


**4. Repeat the steps done in C11 (concept 11) of the tutorial, but set Sx=5*[0.1
0 0 0 0 0; 0 0.1 0 0 0 0; 0 0 4 0 0 0; 0 0 0 0.1 0 0; 0 0 0 0 0.1 0; 0 0 0 0 0
4] and Sz=1*[1 0 ; 0 1]. Explain the differences from the tutorial results.**

When the system noise is increased like this, the observed and predicted values
spread out more around the true state. This makes sense because when the system
noise is increased, our estimate is based more and more on the observation
value.


**5. Repeat the steps done in the previous problem, but set Sx=1*[0.1 0 0 0 0 0;
0 0.1 0 0 0 0; 0 0 4 0 0 0; 0 0 0 0.1 0 0; 0 0 0 0 0.1 0; 0 0 0 0 0 4] and
Sz=5*[1 0 ; 0 1]. Explain the differences from the tutorial results.**

This was way cool, when the observation noise is increased, the observed state
varies a ton, it is all over the place. (Which makes sense, because adding
observation noise beens our observations get worse and worse.) But the Kalman
predicted state was almost spot-on every time!
