<style type="text/css">
body {
    font-size: 10pt;
}
</style>

BZRFlag Tutorial
================

Ben Draut / CS 470-1 / Sept. 25, 2013


**1) Show your partner(s) that you can shoot a stationary tank by typing commands, and print the commands you used, by cutting them from your terminal and pasting them into a document and print it.**

Commands Used:

    ➜  bzrflag git:(master) ✗ telnet localhost 50100
    Trying 127.0.0.1...
    Connected to localhost.
    Escape character is '^]'.
    bzrobots 1
    agent 1
    speed 0 0.5
    angvel 0 0.5
    angvel 0 0
    speed 0 0
    angvel 0 1
    angvel 0 0
    angvel 0 -1
    angvel 0 0
    speed 0 2.0
    speed 0 0
    angvel 0 -0.5
    angvel 0 0
    speed 0 5
    speed 0 0
    angvel 0 0.5
    angvel 0 0
    shoot 0 

**2) Write who your lab partner(s) will be, what language you will be programming in, and what platform (Linux, Windows, etc.) you will be using.**

* **Lab Partner:** None
* **Language:** C++
* **Platform:** Linux
