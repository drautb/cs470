Grid Filter
===========

Ben Draut / CS 470 / Nov. 4, 2013

**1. Step through the MATLAB code for the grid filter. Ignore most of the graphing stuff, but pay attention to the way that observations are made. Discuss how the observation model relates to the likelihood in Bayes rule.**

The likelihood in Bayes rule is the probability of our observation occuring given the state. The observation model in the grid filter example does this by calculating it's beliefs differently depending on whether or not it thinks it observe a building or not.

**2. Try at least five different combinations of the FalseAlarm and TrueHit parameters.**

**What happens as the TrueHit value is lowered and why?**

As the TrueHit value is lowered, it becomes more and more difficult for the sensor to recognize buildings below it. This happens because by decreasing this value, we decrease the probability that the sensor will observe a hit. Lowering this value also decreases the strength of the prior when we apply Bayes rule to improve our estimate.

**What happens when the FalseAlarm parameter is lowered and why?**

As the FalseAlarm value is lowered, the noise in the sensor readings is reduced. (Which makes sense, because the FalseAlarm value determines how frequently the sensor gives false positives.) This happens because reducing the FalseAlarm value reduces the strength of our prior belief that a cell was occupied.

**What happens if FalseAlarm=0 and TrueHit=1, and why?**

We get a perfect sensor. There is no noise in the readings, so we observe buildings exactly where they are. This happens because first, we only observe hits when a building does, in reality, exist at that point, and second, because our belief that a cell has a building is 1 when we have observed a hit, and 0 when we haven't observed a hit. There is no variation.

