Agents Homework
===============

Ben Draut / CS 470-1 / Sept. 18, 2013

**2.4** For each of the following activities, give a PEAS description of the task environment and characterize it in terms of the properties listed in Section 2.3.2.

**Directions:**

Problem 2.4 (in the textbook) JUST SOCCER and BIDDING (the first and the last items in the list of activities): Give a few sentences for each part of the peas model. A little for the parts you find interesting; maybe only one or two sentences for items you think are really obvious.

Repeat the problem given above but instead of doing a PEAS description do an agent-model description -- the one that has states, actions, and consequences (see [Another view of agents](http://students.cs.byu.edu/~cs470ta/goodrich/fall2004/lectures/DecisionTheory.html); include some discussion of goals, preferences, and utilities in addition to the states, actions, and consequences).


## PEAS Description

#### 1) Playing Soccer

**_Performance:_** A good performance measures for a _team_ playing soccer are does it win? Can it's members work together? For a _player_, performance measures could include shooting accuracy, passing accuracy, ability to protect the ball (defense), ability to steal the ball (offense), and situational awareness.

**_Environment:_** The environment for a soccer game includes a field, field areas, goals, a ball, other players, and maybe a referee.

**_Actuators:_** A soccer playing robot would need actuators for steering, moving forward/backwards, kicking the ball, and receiving the ball. The actuator for kicking the ball needs to be able to vary the force with which it "kicks", and the actuator for receiving the ball will need to be able to absorb the impact force a moving ball. If a player passes to a teammate, it could roughly calculate the force needed to cause the ball to _arrive_ at the intended teammate, so the receiver wouldn't need to do much. However, if an opposing player wants to intercept a ball that is being passed, it will need move into the path of the ball, and then absorb the force in order to maintain control.

**_Sensors:_** Camera, GPS, Distance, Impact (detecting when the ball is received, or walls or other players are bumped). A soccer playing robot could use GPS coordinates of all players to determine it's own behavior, but GPS systems are not as fine-grained as would perhaps be desirable for somewhat precise soccer plays. To improve this, the robot would use a camera to recognize the player in question, and the distance sensor to more accurately determine how far away it is or is velocity. It would use the impact sensors to determine if it hit another player or a wall, as well as to help it receive a passed or intercepted ball. 


#### 2) Bidding on an item at an auction

**_Performance:_** Let's assume that we're building a bidding robot. We'll set aside mathematical theory regarding what makes a "good" bid, including profiling/bidding predictions for other participants, and just measure our robot's ability to correctly perceive the environment, and place it's own bids. So, it's performance measures include being able to hear the auctioneer correctly, see the other participant's bids correctly, and place it's own bids.

**_Environment:_** This will include a room or some area that contains an item, an auctioneer, other bidders, and perhaps other auction implements such as a gavel and bidding signs.

**_Actuators:_** A minimal robot would need an actuator to raise it's arm or bidding sign to place a bid. It could also have a speaker to vocalize it's bidding choices as well.

**_Sensors:_** Camera, audio. A bidding robot would need one or multiple cameras in order to see when other participants place bids, and a microphone to detect the audio feedback from the auctioneer regarding the current bid, etc. 


## Agent-Model Description

#### 1) Playing Soccer

**_States:_** The soccer world would have many different states to reflect the state of a match. It would need some kind of _reset_ state to reflect the start of a new match where all agents are reset to their original positions. It could also have a different state for when each team is in possession of the ball, as well as their relative standing. (To reflect when one team has a significant lead over the other, for example.)

**_Actions:_** In this context, actions could correspond to different soccer plays that an agent believes it could make. There could be actions for passing the ball, receiving the ball, shooting the ball, intercepting the ball, etc. There could also be actions for actions related more to the other agents, such as evade, pursue, block, and trip. 

**_Consequences:_** Consequences of various actions would hopefully change the state of the game to help the team that acted. Performing any of the actions would hopefully result in a goal being scored, the ball changing possession, or a penalty being given to the opposing team.

**_Goals:_** A soccer playing agent's primary goal would be to _win the match_ it is playing in. This goal would be broken down into goals such as _score a goal_ and _keep possession of ball_ if the agent's team has possession of the ball, and _prevent a goal_ or _take possession of ball_ if the agent's team is defending. 

**_Preferences:_** When an agent is trying to decide between competing actions, it should take a few different factors into account. If its team has a significant lead, or even a small lead, it should favor maintaining possession of the ball over making a risky move in hopes of furthering its lead. If the game is closer however, an agent might prefer to make riskier moves to increase its lead. 


#### 2) Bidding on an item at an auction

**_States:_** The world of an auction could have a few different states. One state would correspond to the time before an auction begins, another for the time during the auction, (with perhaps another state to reflect the final moments for an item when the auctioneer is waiting for final bids) and a state for the time following the auction. The states could also include some reflection of who is leading the bids, the value of the item being auctioned, and the ferocity of the competition between bidders.

**_Actions:_** A bidding agent could consider only a few different actions. It can either bid, or not. If so desired, it could also consider an action where it bids with the intent to drive the price up, but not to actually win the item. This would be part of a larger goal strategy that would involve several items being auctioned off. An agent may wish to drive up the bids of certain items, so as to eliminate some competition later on when a more desirable item is up for bid.

**_Consequences:_** Consequences of this agent's actions would result either in it taking the lead on a bid, actually winning the item, or nothing happening as it sits passively waiting for the next item.

**_Goals:_** See actions. An agents goals would primarily include winning an item, conserving funds, or driving up a price. (Eliminating competition) It depends on the scope of the auction.

**_Preferences:_** This agents preferences would need to take into account the extent of the auction. If there are certain items that are greatly desired, it would need to prefer conserving funds and driving up prices, over actually acquiring items until something more desirable came up. 


