<style media="screen" type="text/css">
body {
    font-size: 10pt;
}
</style>

Genetic Algorithms Homework
===========================

Ben Draut / CS 470 / October 2, 2013

<!-- **Directions:**

This problem has you experiment with genetic algorithms. Download this MATLAB file. The problem is to find the minimum cost path from the lower left of the graph. Rather than trying to find a specific goal node, the problem is one of trying to find the cheapest path of a fixed length. This simulates flying a fixed-wing UAV in such a way that you stay aloft for a fixed amount of time. The code is written so that edges that are darker are cheaper and such that visiting the same edge twice incurs a very heavy penalty.

The gene is set up so that it encodes a series of directions, N,S, E, or W. Thus, each gene encodes a series of directions and, thus, the edges to traverse through the graph. Genes are initially chosen randomly, but between each generation a new set of genes is constructed. The top NUM_TOP_GENES are preserved from generation to generation. Additionally, the top genes are copied and then mutated with a probability of 1-MUTATION_THRESHOLD. Finally, the top two genes swap heads and tails, and the top gene swaps tails with the third ranked gene. -->

**What happens when you vary the following parameters? Why?**

**1) PATH_LEN (the number of directions in the gene). Try 1 * WORLD_DIM, 2 * WORLD_DIM, and 3 * WORLD_DIM.**

* The algorithm seems to perform the best for shorter path lengths. When the path length starts to exceed the world dimensions, it has a hard time finding a path that doesn't backtrack on itself and incur a penalty. For 1 * WORLD_DIM, the cost was usually around 20. For 2 * WORLD_DIM, the cost went up to around 50 or 60, but for 3 * WORLD_DIM, it shot up above 130. 

* I attribute this to the fact that a GA doesn't lend itself too well to this kind of pathfinding problem. Because the encoding isn't "intelligent" enough to know anything more or less than if the cost goes up or down, it can't intelligently choose a path whose cost may be higher initially, but it gets away from the backtracking penalty.

**2) NUM_GENES (the number of different genes considered). Try 10, 50, and 100.**

* As NUM_GENES inceases, the algorithm, on average, is able to find lower cost paths more frequently. When there are only 10 or even 50 genes, in most cases, it couldn't find a path that cost less than 60 or 70. Increasing the number of genes to 100 caused it to find paths around 50 or 55. 

* This is because as we increase the number of genes, we increase the probability that a lower-cost chromosome will show up at some point. The greater the population size, the greater the diversity, and the greater the chance of finding a lower-cost path.

**3) MAX_GENERATIONS (how many generations the code runs). Try 30, 60, and 120.**

* Increasing the number of generations improves the average performance of the algorithm. With only 30 generations, it would sometimes find a really good solution, (45-50ish) but most of the time it would find one in the 60's or 70's. Increasing the number of generations to 60 helped this, it started getting solutions in the 50's around half the time. The best improvement was when the number of generations was 120. With 120 generations, the algorithm would find a path with cost < 50 in 90% of the simulations I ran.

* Increasing the number of generations basically increases the runtime of the algorithm. A genetic algorithm will always return the best solution it has found so far whenever it stops. The longer it runs, the greater the probability of it developing an optimal gene. So, when we run it with 120 generations, we give it more time to explore the search space.
