<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
body {
    font-size: 10pt;
}
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Utility Theory Homework
=======================

Ben Draut / CS 470 / December 9, 2013

**Consider the problem of detecting an incoming missile given an alarm. The  alarm is either on (corresponding to percept x = 1) or off (corresponding to percept x = 0). The world is such that there is either a missile (corresponding to state $\theta$ = 1) or no missile (corresponding to state $\theta$ = 0). You know the following probabilities:**

|                                  |                                  |
| -------------------------------- | -------------------------------- |
| $P(x = 0&#124;\theta = 0) = 0.9$ | $P(x = 0&#124;\theta = 1) = 0.2$ |
| $P(x = 1&#124;\theta = 0) = 0.1$ | $P(x = 1&#124;\theta = 1) = 0.8$ |
| $P(\theta = 0) = 0.7$            | $P(\theta = 1) = 0.3$            |

**An alarm occurs. What is the most likely explanation? (To get full credit, you must show your work.)**

$P(\theta|x) = \frac{P(x|\theta)P(\theta)}{P(x)}$

$\frac{P(x|\theta)P(\theta)}{P(x)} = \frac{(0.8)(0.3)}{(0.1 + 0.8)} = 0.2667$

There is only a 27% chance that there actually was a missile, given that the alarm went off, therefore it is more likely that it was a false alarm.

**Consider a problem similar to the missile detection problem above. Suppose that you make an observation $x = 1$ (the alarm has gone off) and determine that the probability of an incoming missile given the observation is $P(\theta = 1|x = 1) = 0.01$, and the probability of a false alarm given the observation is $P(\theta = 0|x = 1) = 0.99$. You have two actions that you can take: $u_1$ is the option of launching a defensive weapon to defuse the missile, and $u_0$ is the option of doing nothing. You have constructed the following utilities: $U(u_0;\theta = 0) = 0.5$, $U(u_1;\theta = 0) = 0.4$, $U(u_0;\theta = 1) = 0.0$, and $U(u_1;\theta = 1) = 8.0$. For example, $U(u_0;\theta = 0)$ is the utility of doing nothing when no missile is coming in. What is the optimal solution? (Hint, you must show how you derive the utility of both options to get credit.)**

$a* = \underset{u_0, u_1}{argmax} \sum_{u_0, u_1}^{} U(a,s)P(s)$

For $u_0$: $U(u_0, \theta = 1)P(\theta = 1) + U(u_0, \theta = 0)P(\theta = 0)$

$u_0 = (0.0)(0.01) + (0.5)(0.99) = 0.495$

For $u_1$: $U(u_1, \theta = 1)P(\theta = 1) + U(u_1, \theta = 0)P(\theta = 0)$

$u_1 = (1)(0.01) + (0.4)(0.99) = 0.406$

The optimal solution (or the one tha maximizes expected utility) is $u_0$, or doing nothing.

**Using the preferences among lotteries technique, build a utility function over the set of alternatives ${A, B, C, D}$ given the following constraints:**

* $A \succ B \succ C \succ D$
* $U(A) = 200$
* $U(D) = 40$
* $B \sim [ \frac{4}{5}, A; \frac{1}{5}, C]$
* $C \sim [ \frac{1}{4}, B; \frac{3}{4}, D]$

**Please carefully read the lotteries and note that these constraints state that we are indifferent between B and a lottery in A and C (not in A and D), and indifferent between C and a lottery in B and D (not in A and D).**

Using the formula from class, we obtain:

$U(B) = (\frac{4}{5})(200) + (\frac{1}{5})(U(C))$ and

$U(C) = (\frac{1}{5})(U(B)) + (\frac{3}{4})(40)$.

Substituting the equation for $U(C)$ in the equation for $U(B)$, we obtain:

$U(B) = (\frac{4}{5})(200) + (\frac{1}{5})((\frac{1}{5})(U(B)) + (\frac{3}{4})(40))$

Which reduces to:

$U(B) = 172.91667$ or about $173$.

Substituting this back into the original equation for $U(C)$, we obtain:

$U(C) = (\frac{1}{5})(172.91667) + (\frac{3}{4})(40)$

Which reduces to:

$U(C) = 64.58333$ or about $65$.

This gives the final utility values:

* $U(A) = 200$
* $U(B) = 173$
* $U(C) = 65$
* $U(D) = 40$
