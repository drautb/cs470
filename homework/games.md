<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Game Theory Homework
====================

Ben Draut / CS 470 / Dec. 2, 2013

**Directions**

The following table represents the payoﬀs for a zero sum game with two players:
$P1$ and $P2$. Let $P1$ be the row player and $P2$ be the column player.


|        | b1     | b2     | b3     | b4     |
| ------ | ------ | ------ | ------ | ------ |
| **a1** | (1,-1) | (0,0)  | (-1,1) | (1,-1) |
| **a2** | (0,0)  | (-2,2) | (2,-2) | (4,-4) |
| **a3** | (-3,3) | (2,-2) | (1,-1) | (0,0)  |
| **a4** | (-1,1) | (0,0)  | (7,-7) | (-9,9) |

The ordered pairs in the payoff matrix show $(u1(a, b), u2(a, b))$. $P1$
performs actions $ai;i = 1,2,3,4$ and $P2$ performs actions $bi;i = 1,2,3,4$.

**What is the maximin value for $P1$?**

The maximin value is $v^{MM}_{P1} = -1$

Which corresponds to the solution $a^{MM}_{P1} = a1$

**What is the maximin solution for $P2$?**

The maximin value is $v^{MM}_{P2} = -1$

Which corresponds to the solution $a^{MM}_{P2} = b1$
