<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<style type="text/css">
.img-container {
    width: 100%;
    text-align: center;
}

.img-container img {
    /*width: 120px;*/
}
</style>

Bayesian Nets
=============

Ben Draut / CS 470 / Oct. 25, 2013

**1. Consider the Bayesian network and conditional probability tables shown in Figure 14.2 on page 512. Compute the following:**

**a. The probability that John calls, Mary does not call, the Alarm goes off, that a burglary is occuring, and that no earthquake is occurring.**

Compute $P_{J,M,A,B,E}(T,F,T,T,F)$.

$P_{J,M,A,B,E}(T,F,T,T,F) = P_{J|A}(T|T) * P_{M|A}(F|T) * P_{A|B,E}(T|T,F) * P_{B}(T) * P_{E}(F)$

$P_{J,M,A,B,E}(T,F,T,T,F) = (0.90)(0.30)(0.94)(0.001)(0.998)$

$P_{J,M,A,B,E}(T,F,T,T,F) = 0.0002533$ or, less than $0.03\%$.

**b. The probability that John and Mary both call given that a burglary is going on and that no earthquake is happening.**

Compute $P_{J,M|B,E}(T,T|T,F)$

$P_{J,M|B,E}(T,T|T,F) = \sum_{A}^{} P_{J,M,A|B,E}(T,T,a|T,F)$

$\sum_{A}^{} P_{J,M,A|B,E}(T,T,a|T,F) = P_{J,M,A|B,E}(T,T,T|T,F) + P_{J,M,A|B,E}(T,T,F|T,F)$

First Half:

$P_{J,M,A|B,E}(T,T,T|T,F) = P_{J|A}(T|T) * P_{M|A}(T|T) * P_{A|B,E}(T|T,F)$

$P_{J,M,A|B,E}(T,T,T|T,F) = (0.90)(0.70)(0.94) = 0.5922$

Second Half:

$P_{J,M,A|B,E}(T,T,F|T,F) = P_{J|A}(T|F) * P_{M|A}(T|F) * P_{A|B,E}(F|T,F)$

$P_{J,M,A|B,E}(T,T,F|T,F) = (0.05)(0.01)(0.06) = 0.00003$

Together:

$P_{J,M|B,E}(T,T|T,F) = 0.5922 + 0.00003 = 0.59223$ or, about $59\%$.

**c. The probability that an alarm is not on given that an earthquake is happening and no burglary is happening.**

Compute $P_{A|E,B}(F|T,F)$

$P_{A|E,B}(F|T,F) = 1 - P_{A|E,B}(F|T,F)$

$P_{A|E,B}(F|T,F) = 1 - 0.29$

$P_{A|E,B}(F|T,F) = 0.71$

**3. Draw the Bayesian network associated with the following joint distribution function: $P(A,B,C,D)=P(A)P(B|A)P(C|B)P(D|B)$.**

<div class="img-container">
    <img src="../../Images/bayes-net-1.png">
</div>

**3. Draw the Bayesian network associated with the following joint distribution function: $P(A,B,C,D)=P(A)P(B)P(C|A,B)P(D|C)$.**

<div class="img-container">
    <img src="../../Images/bayes-net-2.png">
</div>

**4. Create a simple Bayesian network for a problem with at least four variables.**

**a. Draw the network**

<div class="img-container">
    <img src="../../Images/bayes-net-biking.png">
</div>

* $O$ is the probability that my bike is in working order.
* $S$ is the probability that it is sunny outside.
* $B$ is the probability that I go mountain biking.
* $A$ is the probability that I have an accident.

**b. Specify the conditional probability tables for your network.**

<div class="img-container">
    <img src="../../Images/biking-dist.png">
</div>

**c. Justify the values in your tables.**

At any given moment, there is a 98% chance that my bike is in working order. On any given day, there is a 95% chance that it will be sunny outside. (I made this up, but it's realistic and could be obtained from historical records.) Given that it is sunny out and my bike is in working order, there is a 75% chance that I will go mountain biking. If it's not sunny, but my bike is still in good shape, there is still a 35% chance that I'll go biking. If it's sunny out, but my bike isn't in working order, there is only a 5% chance that I'll still go biking. I'd have to get parts, repair things, etc. though, and sometimes good weather is motivation enough to do that. If it isn't even sunny though, there's only a 2% chance that I'll do it. Now if I do go biking, there is a 5% chance that I'll get in an accident. If I decide to stay home or do something else though, there is a much smaller chance that I'll get in an accident. (0.1%)


