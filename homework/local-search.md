<!-- These two scripts are necessary for MathJax to parse the Latex equation code -->
<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
<!-- This spaces out the table columns that MD generates -->
<style type="text/css">
table tr:nth-child(2n) {
    background-color: #f8f8f8;
}
table, th, td {
    border: 1px solid #ddd;
    padding: 6px 13px;
}
</style>

Local Search Homework
=====================

Ben Draut / CS 470-1 / Sept. 30, 2013

## Part 1

**1) Suppose $f(x) = -3(x-2)^2$. At what value of $x$ does $f(x)$ take its maximum value?**

$f(x) = -3(x-2)(x-2) = -3x^2+12x-12$

$f'(x) = -6x + 12$

Setting $f'(x) = 0$, solving for $x$ gives $x = 2$

So, $f(x)$ is maximized when $x = 2$. $f(2) = -3(2-2)^2 = 0$

<br/>
**2) Suppose that we use gradient ascent (from page 131 and 132 of the book) to find the maximum of the function from problem 1. Let $α = 0.1$ and suppose that our first guess of the maximum is $x = 0$. What is our guess after one iteration of applying the equation at the top of page 132? What is our guess after two iterations of applying the algorithm? (Hint: be careful that you carefully plug your answer from the first iteration into the update for the second iteration.)**

Given that $α=0.1$ and $∇f(x) = -6x + 12$

| Iteration | Guess (x) | f(x)   | New Guess  |
| --------- | --------- | ------ | ---------- |
| 1         | 0         | -12    | 0 + (0.1)(-6(0) + 12) | 
| 2         | 1.2       | -1.92  | 1.2 + (0.1)(-6(1.2) + 12) | 
| 3         | 1.68      | -0.3072| ... |

<br/>
**3) Compute the gradient of $f(x,y) = 3x^2y^3 − x^2 + 5y^2$ by computing the partial derivatives  $\frac{\partial f}{\partial x}$  and  $\frac{\partial f}{\partial y}$. (See [this link](http://en.wikipedia.org/wiki/Partial_derivative) for hints.)**

$\frac{\partial f}{\partial x} = 6xy^3-2x$

$\frac{\partial f}{\partial y} = 9x^2y^2+10y$

Therefore:

$∇f(x,y) = [6xy^3-2x, 9x^2y^2+10y]$

<br/>
**4) Suppose that we use gradient ascent (from page 131 and 132 of the book) to find the maximum of $f(x,y) = − (x − 3)^2 − 5y^2 + 2xy$. Let $α = 0.05$ and suppose that our first guess of the maximum is $x = 0,y = 1$. What is our guess after one iteration of applying the equation at the top of page 132? What is our guess after two iterations of applying the algorithm? (Hint: Notice that this equation is different from the one in the previous problem.)**

Given $f(x,y) = −(x − 3)^2 − 5y^2 + 2xy$ and $α = 0.05$

$∇f(x,y) = [2y-2x+6, 2x-10y]$

| Iteration | Guess (x,y) | f(x,y) | New Guess   |
| --------- | ----------- | ------ | ----------- |
| 1         | [0, 1]      | -14    | [0.4, 0.5]  |
| 2         | [0.4, 0.5]  | -7.61  | [0.71, 0.29]|
| 3         | [0.71, 0.29]| -5.253 | ...         |


## Part 2

**Directions:**

The attached Local Search MATLAB code has you play with various local search algorithms: hill-climbing, random restart hill-climbing, and beam-forming. 

The code is organized as follows:

* hill climbing on a surface with one bump
* hill climbing on a surface with more than one bump
* random restart hill climbing on a surface with more than one bump
* local beam search on a surface with more than one bump

Run the code so that you learn how hill-climbing, random restart hill-climbing, and local beam search differ. Then, answer the following questions:

**1) What is the probability that hill climbing will reach the maximum point on a surface with more than one bump? How does this probability depend on the footprint of the hill?**

Hill climbing is guaranteed to reach the maximum point on a surface if it begins somewhere on the uphill slope of the bump containing the maximum point of the surface, given that the slope is uphill all the way to the top. (If there are smaller bumps on the slope of the larger bump, it will get stuck.) So, the probability that hill climbing will reach the maximum point on surface is the same as the probability that a randomly chosen starting point lies somewhere on a continuously uphill slope of the maximum bump.


**2) When would you perform random restart hill-climbing rather than hill-climbing? How would you know which to run?**

It depends on the landscape of your search space. If the landscape has a lot of variance, random restart hill climbing is a good choice because it increases your probability of starting on one of the up-slopes that goes continuously up to the global maximum. If the landscape is less varied, meaning that most, if not all, of the time you will start on an up-slope that leads to the global maximum, then you don't need to restart. It can also depend on the cost of computation. If calculating neighbors or their value is expensive, then you'll want to avoid random restart. If you don't know the nature of the landscape though, you need to consider it.

**3) What would happen if you changed the beam search algorithm so that it took the 10 best solutions instead of the 7 best? Why?**

You would increase the computation time, because you would have to compute more neighbors. (10*5 as opposed to 7*5 successor states) The benefit however, would be that you increase your probability of finding one of global maxima, because you increase your initial coverage of the search space.
