Towards Sequential Reasoning
============================

_Bayesian Reasoning MATLAB Tutorial_

Ben Draut / CS 470 / Oct. 25, 2013

**Key Points:**

* In real-world applications, determining an exact state is difficult.
* We can estimate the state, and improve our estimate by combining previous observations with what we know about the noise in our sensor. 
* We can calculate likelihoods and use previous observations as priors, and then use Bayes rule to get a more informed estimate, which is often better than simply averaging previous observations. (We use all the information available to us when we use Bayes rule.)

**Muddy Points:**

I understand the big picture I think, but I'm still shaky on the concrete math. 

