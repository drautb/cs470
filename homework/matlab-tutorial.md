CS 470 MATLAB Tutorial
======================

Ben Draut / CS 470-1 / Sept. 9, 2013

## Plot
**Fig. 1:**
![](plot-1.png)

## Histograms: Default Bucket Count
**Fig. 2-1:** *hist(x)* when *(a, b) = (1, 0)*
![](hist-x.png)

**Fig. 2-2:** *hist(y)* when *(a, b) = (10, 0)*
![](hist-y.png)

**Fig. 2-3:** *hist(z)* when *(a, b) = (1, -4)*
![](hist-z.png)

**Fig. 2-4:** *hist(a)* when *(a, b) = (10, 30)*
![](hist-a.png)

### Explanation
The MATLAB function *randn* returns random values based on a normal distribution.
Because of this, each of our four diagrams resembles a normal distribution, that is,
it is roughly bell-shaped, and nearly 100% of all values lie between -3 and 3.
The range of the distribution is expanded by a factor of *a*. (This can be seen in
figure 2-4, where *a = 10*.) The mean of the distribution is approximately *b*, thus *b* shifts
the distribution without enlarging or shrinking it.

## Histograms: 100 Buckets
**Fig. 3-1:** *hist(x, 100)* when *(a, b) = (1, 0)*
![](hist-x-100.png)

**Fig. 3-2:** *hist(y, 100)* when *(a, b) = (10, 0)*
![](hist-y-100.png)

**Fig. 3-3:** *hist(z, 100)* when *(a, b) = (1, -4)*
![](hist-z-100.png)

**Fig. 3-41:** *hist(a, 100)* when *(a, b) = (10, 30)*
![](hist-a-100.png)

## Histogram Comparison
The primary difference between the two sets of histograms is granularity. The first set
of histograms, for which MATLAB selects the appropriate number of buckets, is very coarse.
The second set however, is much more fine-grained. It is easier to see a more exact
representation of where the values were distributed.
