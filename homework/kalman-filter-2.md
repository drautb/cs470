<script type="text/x-mathjax-config">MathJax.Hub.Config({tex2jax: {inlineMath: [['\$','\$']]}});</script>
<script type="text/javascript"src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

Second Kalman Filter Homework
====================

Ben Draut / CS 470 / Dec. 2, 2013

## Part 1

**The following four questions ask you to interpret the update equations for the Kalman filter under the special circumstances where $Σ_0$ and $Σ_x$ contain only zeros.**

**What does the Kalman gain become when $Σ_0$ and $Σ_x$ contain only zeros?**

$K_{t + 1} = (FΣ_tF^T + Σ_x)H^T(H(FΣ_tF^T + Σ_x)H^T + Σ_z)^{−1}$

When $Σ_0$ and $Σ_x$ contain only zeros, it means that there is no noise in our state model, or that our state prediction is perfect. If this is the case, then the Kalman gain will go to zero, because the observation will contain no additional information. We can trust the state model completely.

**What does this equation become when $Σ_0$ and $Σ_x$ contain only zeros?**

$μ_{t + 1} = Fμ_t + K_{t + 1}(z_{t + 1} − HFμ_t)$

Becomes:

$μ_{t + 1} = Fμ_t$

Again, because there is no noise in the state model, we can trust it completely. There is no need for the Kalman gain and correction term.

**What does this equation become when $Σ_0$ and $Σ_x$ contain only zeros?**

$Σ_{t + 1} = (I − K_{t + 1}H)(FΣ_tF^T + Σ_x)$

Becomes:

$Σ_{t + 1} = FΣ_tF^T$

In other words, the covariance becomes dependent only upon the previous covariance, which only included the noise in the sensor model. (There was no noise in the state model.)


## Part 2

**Suppose that we modify the system model from the Kalman filter equations to allow a control variable, giving $x_{t + 1} = Fx_t + Gu_t + η_t$, with $η_t \sim N(0,Σ_x)$, where $u_t$ denotes the control variable.**

**How would you expect the Kalman filter equation for $μ_{t + 1}$ to change? Why?**

I would expect it to become:

$μ_{t + 1} = Fμ_t + Gu_t + K_{t + 1}(z_{t + 1} − HFμ_t)$

The equation would just need another term to take the control variable into account in its calculations.

**How would you expect the Kalman filter equation for $Σ_{t + 1}$ to change? Why?**

I wouldn't expect the equation to change at all. I believe that any additional noise added by the control variable would be accounted for in $Σ_x$, so no modifications to the equation would need to be made.

